; convert the flux from Jy to W/m^2/um
;
; flux must be an array(n,i)
; where n=0 is the wavelength in m
;
; all the columns secified by the NCOL keword are converted
; if NCOL is not given only the 2nd column (n=1) will be
; processed
;
; USAGE
;   print, Jy2W([10., 1.1691698e-012])
;
; IDL will print: 10.000000  38.999999
;
function jy2w, w, flux, um=um
  c = 3.0e8
  h = 6.62565e-34
  f=flux*1e-26*c/(w * w)
  if keyword_set(um) then  f=f*1e-6
  return, f
end
