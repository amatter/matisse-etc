

;!PATH=!PATH+':'+expand_path('+'+'/home/mvannier/IDLtools/oifits_lib/OI-data/')
;!PATH=!PATH+':'+expand_path('+'+'/home/mvannier/IDLtools/oifits_lib/pro/')
;print, !path

function struc_function, nPb, t
  ;-----------------------------------
  ; Compute structure function of the background fluctuation 
  ; in function of background photon number of the photometric channel and time
  ;-----------------------------------
  
  Db = 2*((nPb/1000)^2)*(1-exp(-0.693*t))
  
  return, Db
end

;------------------------------------------------------------------
;Maybe add another function calculating the transmission parameters
;(chromatic overall transmission for a stellar source and for the
;thermal background for each spectral band and each telescope
;------------------------------------------------------------------


function bg_trans,trans_cop,trans_p,trans_SB
;-----------------------------------------------------------
;Compute the overall transmission for the thermal background
;-----------------------------------------------------------

;----------------
;Input parameters
;----------------
;trans_cop: transmission of MATISSE cold optics 
;trans_p: transmission of the polarization selector 
;trans_SB: transmission of the spatial filter assembly

trans_bg=trans_cop*trans_p*trans_SB

return,trans_bg
end


function emiss_bg,Ta,TI,TW,Cp
;-----------------------------------------------------------------------------------------
;Compute the global emissivity of the optical train (thermal background, optical elements)
;-----------------------------------------------------------------------------------------

;----------------
;Input parameters
;----------------
;Ta : atmospheric transmission 
;T1: transmission of the interferometer down to the feeding optics inclusively
;Tw: transmission of the MATISSE warm optics
;Cp: coefficient due to pupil motion 

epsilon_bg=Cp*(1.-Ta*TI*TW)

return,epsilon_bg
end

function trans_source,SR,Ta,T1,Tw,Tc,Tp,Ts
;--------------------------------------------------------------------
;Compute the overall transmission of the sky source (TO BE COMPLETED)
;--------------------------------------------------------------------

;SR: Strehl ratio at the spatial filter image plane
;Ta: atmospheric transparency
;T1: transmission of the interferometer down to the feeding optics
;inclusively
;Tw: transmission of the MATISSE warm optics
;Tc: transmission of the MATISSE cold optics
;Tp: transmission of the polarization selector if any
;Ts:Transmission of the spatial filter assembly (should be chromatic across one
;spectral band given the fixed size of the masks and the pinhole)

T_source=SR*Ta*T1*Tw*Tc*Tp*Ts

return,T_source

end

function compute_lambda_matisse,tab_lambda_pix,lambda_ref,pinhole_size
;--------------------------------------------------------------
;Compute the wavelength array (in m) corresponding to a given spectral
;resolution (central wavelength and spectral channel width)
;--------------------------------------------------------------

;tab_lambda_pix: MATISSE wavelength table 
;lambda_ref: reference wavelength at which we know the width of a
;spectral channel (3.2um in LM band and 8um in N band)
;pinhole_size : size of the spatial filter in unit of lambda/D

nlambda_pix=n_elements(tab_lambda_pix)
tab_pix=findgen(nlambda_pix)
delta_lambda=make_array(nlambda_pix,/double)
delta_lambda_pix=make_array(nlambda_pix,/double)
tab_lambda=make_array(nlambda_pix,/double)
delta_lambda_pix[0]=abs(tab_lambda_pix[1]-tab_lambda_pix[0])
for i=1,nlambda_pix-1 do delta_lambda_pix[i]=abs(tab_lambda_pix[i]-tab_lambda_pix[i-1])
n=0 ;spectral channel
p=0d ;pixel
np=0d
tab_lambda(0)=tab_lambda_pix(0)
delta_lambda[0]=(tab_lambda_pix[0]/lambda_ref)*3.d*pinhole_size
repeat begin
      n=n+1
      np=p+delta_lambda[n-1]
      np1=floor(np)
      np2=ceil(np)     
      tab_lambda[n]=interpol(tab_lambda_pix,tab_pix,np); 
      delta_lambda[n]=(tab_lambda[n]/lambda_ref)*3.d*pinhole_size ;Spectral channel width at the 'tab_lambda_pix' wavelength 
      p=np
endrep until (np + delta_lambda[n]) gt nlambda_pix  

tab_lambda[n] = 0
delta_lambda[*] = 0
n = n - 1

tab_lambda_final=make_array(n,/double)
delta_lambda_final=make_array(n,/double)
delta_lambda_final[0] = abs(tab_lambda[1] - tab_lambda[0] )
tab_lambda_final[0] = (tab_lambda[1] + tab_lambda[0] ) * 0.5  ;Central wavelength
for i=1,n-1 do begin
   delta_lambda_final[i] = abs(tab_lambda[i] - tab_lambda[i-1] )
   tab_lambda_final[i] = (tab_lambda[i] + tab_lambda[i-1] ) * 0.5 ;Central wavelength
endfor

lambda_matisse=CREATE_STRUCT('eff_wave',tab_lambda_final*1e-6,'eff_band',delta_lambda_final*1e-6)

return,lambda_matisse

end


;function trans_sf,wavelength,pinhole_size
;---------------------------------------------------------------------
;Compute the transmission of the spatial filter assembly as a function
;of wavelength (TO BE COMPLETED) 
;---------------------------------------------------------------------

;wavelength: spectral channel wavelength
;pinhole_size: physical size of the pinhole (image cold stop)


;T_SF=1

;return,T_SF

;end


function blackbody,lam,temp

  h=6.62607004e-34 ;in Joule.second
  c=2.99792458e8 ;in meter/second
  k=1.38064852e-23 ;in Joule/Kelvin

  ;-----------------------------------
  ; Compute the blackbody flux for a temperature 'temp' and a (or an array of)
  ; wavelength 'lambda'. Flux in W/m²/micron
  ;-----------------------------------

  blackbody = (2*h*c^2/lam^5)*(1./(exp((h*c)/(lam*k*temp))-1)) ;en W/m2/m1/sr. 
  return,blackbody

end


pro snr_analysis_chromatic_v9, tab_lambda, vis, source_flux,error_v,error_v2,error_c,error_phi, ADD_TRANS=ADD_TRANS,FIXED_WINDOW=FIXED_WINDOW,NOISE_PER_PIX=NOISE_PER_PIX,MAG_SOURCE=MAG_SOURCE,TAB_LAM_MATISSE=TAB_LAM_MATISSE
;------------------------------------------------------------------------------------------------
; To perform the SNR analysis on the visibility, the normalized Fourier Transform of an image and
; the flux in Jansky in a given spectral band is needed
;------------------------------------------------------------------------------------------------

;------------------
;Input parameters
;------------------

;tab_lambda : array of wavelengths (in m)
;vis : Chromatic visibility of the source (must have same dimension as tab_lambda)
;source_flux: Flux of the source as a function of wavelength in the
;desired spectral band (in Jy; must have same dimension as tab_lambda)
;error_v: returned error on the calibrated visibility 
;error_c: returned error on the closure phase (including for the moment only fundamental noises)
;error_phi: returned error on the differential phase (including for the moment only fundamental noises)
;ADD_TRANS: Keyword to include a atmospheric transmission model as expected in Paranal in the case of median humidity conditions (pwv=2.5mm)
;FIXED_WINDOW: if selected, the routine will consider a fixed window size in the spatial direction (as currently considered in the DRS)
;NOISE_PER_PIX: if selected, the routine returns errors per spectral pixel and not per spectral channel
;MAG_SOURCE: if selected, the source brightness has to be entered in magnitude and not in flux (Jy)
;TAB_LAM_MATISSE: if selected, the different SNRs are computed at the MATISSE wavelengths, instead of the wavelength array entered as input by the user 



;--------------------------------------------------------------------------------------------------------
;We first call the list of parameters written in the files param_matisse_expert.pro and param_matisse.pro
;--------------------------------------------------------------------------------------------------------
@param_matisse_expert_v2.pro
@param_matisse_v2.pro
;common param_expert
;common param


 
;--------------------------
;nb_photon parameters
;-------------------------- 
h_planck=6.62607004e-34 ;in Joule.second
kb=1.38064852e-23 ;in Joule/Kelvin
c=2.99792458e8 ;in meter/second
nw=n_elements(tab_lambda)
nvis=n_elements(vis) 


;-----------------------------------------------------------------------
;Total frame time in CDS readout mode, which includes the double read
;of the detector. In N-band, the detector readout speed is high,
;which makes usually possible, especially in low spectral resolution, an
;additional integration time. In L-band, without fringe tracker, 
;no additional integration time should be possible. In practice, the
;DIT in L band, limited by the atmospheric coherence time, will
;correspond to the minimum time to read the detector.   
;-----------------------------------------------------------------------

DIT=det_time_params[0]
t_exp_LM=det_time_params[1]
t_exp_N=det_time_params[1]

;t_exp_LM = deadtime_LM + 2.*DIT ;Total frame time for the HAWAII detector
;t_exp_LM = overheads_factor_LM*DIT ;Total frame time for the HAWAII detector
;if strcmp(Res_N,'N_high',/FOLD_CASE) eq 1 then t_exp_N = deadtime_N + 2.*DIT else t_exp_N = deadtime_N + DIT + readout_time_N ;Total frame time for the AQUARIUS detector
;t_exp_N = overheads_factor_N*DIT ;Total frame time for the AQUARIUS detector

;-------------------------------
;Flux fraction and exposure time
;-------------------------------

if mode eq 0 then begin ;SiPhot mode
  alpha_I = 2./3. ;proportion of flux in the interferometric channel
  alpha_P = 1./3. ;proportion of flux in the photometric channel

  ;-----------------------
  ;Maybe to be modified
  ;-----------------------
  texp_LM_C = t_exp_LM  ;Exposure time corresponding to the required number of frame acquisitions for one coherent flux estimation
  texp_N_C = t_exp_N  ;Exposure time corresponding to the required number of frame acquisitions for one coherent flux estimation
  texp_LM_n = t_exp_LM  ;Exposure time corresponding to the required number of frame acquisitions for one photometry estimation
  texp_N_n = t_exp_N  ;Exposure time corresponding to the required number of frame acquisitions for one photometry estimation
  texp_LM_phi = texp_LM_C ;Total exposure time associated with one phase and closure estimation
  texp_N_phi = texp_N_C   ;Total exposure time associated with one phase and closure estimation
endif 

if mode eq 1 then begin ;HighSens mode
  alpha_I = 1. ;proportion of flux in the interferometric channel
  alpha_P = 1. ;proportion of flux in the photometric channel

  ;-----------------------
  ;Maybe to be modified
  ;-----------------------
  texp_LM_C = t_exp_LM  ;Exposure time corresponding to the required number of frame acquisitions for one coherent flux estimation
  texp_N_C = t_exp_N  ;Exposure time corresponding to the required number of frame acquisitions for one coherent flux estimation
  texp_LM_n = t_exp_LM  ;Exposure time corresponding to the required number of frame acquisitions for one photometry estimation (Geometric mean of the photometry of two beams)
  texp_N_n = t_exp_N  ;Exposure time corresponding to the required number of frame acquisitions for one photometry estimation (Geometric mean of the photometry of two beams)
  ;if HighSens_photometry eq 1 then begin ;photometry measurement after fringes
  ;   texp_LM_phi = (n_T+1)*texp_LM_C ;Total exposure time associated with one phase and closure estimation
  ;   texp_N_phi = (n_T+1)*texp_N_C   ;Total exposure time associated with one phase and closure estimation
  ;endif else begin ;no photometry measurement after fringes
  texp_LM_phi = texp_LM_C ;Total exposure time associated with one phase and closure estimation
  texp_N_phi = texp_N_C   ;Total exposure time associated with one phase and closure estimation
  ;endelse

endif



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
;SNR and errors calculation in LM band
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
if tab_lambda(nw-1) lt 5.5e-6 then begin

;-----------------------------------------------
;Interpolation over the correct wavelength array
;-----------------------------------------------
lambda_ref=3.2
s='tab_lambda_pix=disp_'+Res_LM
b=Execute(s) ;Wavelength array in pixel
print,'tab_lambda_pix = ',reverse(tab_lambda_pix)

if keyword_set(NOISE_PER_PIX) then begin
   nlambda_pix=n_elements(tab_lambda_pix)
   tab_lambda_pix=reverse(tab_lambda_pix)*1e-6
   delta_lambda_pix=make_array(nlambda_pix,/double)
   delta_lambda_pix[0]=abs(tab_lambda_pix[1]-tab_lambda_pix[0])
   for i=1,nlambda_pix-1 do delta_lambda_pix[i]=abs(tab_lambda_pix[i]-tab_lambda_pix[i-1])
   delta_lambda=interpol(delta_lambda_pix,tab_lambda_pix,tab_lambda)
   if keyword_set(TAB_LAM_MATISSE) then begin
      tab_lambda=tab_lambda_pix
      delta_lambda=delta_lambda_pix
      source_flux=replicate(source_flux[0],n_elements(tab_lambda))
   endif
   print,'delta_lambda = ',delta_lambda

endif else begin

   tab_lambda_matisse=compute_lambda_matisse(reverse(tab_lambda_pix),lambda_ref,pinhole_size_LM)
   ;print,'tab_lambda_matisse = ',tab_lambda_matisse.eff_wave
   ;print,'delta_lambda_matisse = ',tab_lambda_matisse.eff_band
   delta_lambda=interpol(tab_lambda_matisse.eff_band,tab_lambda_matisse.eff_wave,tab_lambda)
    if keyword_set(TAB_LAM_MATISSE) then begin
      tab_lambda=tab_lambda_matisse.eff_wave
      delta_lambda=tab_lambda_matisse.eff_band
      source_flux=replicate(source_flux[0],n_elements(tab_lambda))
   endif  
   print,'delta_lambda = ',delta_lambda
endelse

if keyword_set(NOISE_PER_PIX) then begin
;------------------------------------------
;Number of pixels read per spectral column
;------------------------------------------

if keyword_set(FIXED_WINDOW) then begin
   n_pix_I_32um=625.            ;nb of pixels read per spectral column at 3.2um in the interferometric channel (fixed window size in the spatial direction)  
   n_pix_P_32um=150.            ;nb of pixels read per spectral column at 3.2um in a photometric channel (fixed window size in the spatial direction)  
endif else begin
   n_pix_I_32um=288.            ;nb of pixels read per spectral column at 3.2um in the interferometric channel (lambda-dependent window size in the spatial direction) 
endelse
endif else begin

;------------------------------------------
;Number of pixels read per spectral channel
;------------------------------------------
if keyword_set(FIXED_WINDOW) then begin
   n_pix_I_32um=2812.            ;nb of pixels read per spectral channel at 3.2um in the interferometric channel (fixed window size in the spatial direction) 
   n_pix_P_32um=675.            ;nb of pixels read per spectral channel at 3.2um in a photometric channel (fixed window size in the spatial direction)  
endif else begin
   n_pix_I_32um=1296.            ;nb of pixels read per spectral channel at 3.2um in the interferometric channel (lambda-dependent window size in the spatial direction) 
endelse

endelse



;------------
;Solid angle
;------------
delta_Omega_LM_AT = (!dpi/4.)*(pinhole_size_LM*3.5e-6/(D_tel_AT))^2 ;it corresponds to the development of 1-cos(x/2) when x is very small
delta_Omega_LM_UT = (!dpi/4.)*(pinhole_size_LM*3.5e-6/(D_tel_UT))^2 ;it corresponds to the development of 1-cos(x/2) when x is very small

;-----------------------------
;L band
;-----------------------------
index=where(tab_lambda lt 4.4e-6,count1)
iL=where(tab_lambda[index] gt 2.8e-6,count)
if count eq 0 then begin
   print,'you are out of the L band (2.8 um - 4.2 um)'
   stop
endif
if count1 ne 0 then begin
   tab_lambda_L=tab_lambda(index[iL])
   delta_lambda_L=delta_lambda(index[iL])
   source_flux_L=source_flux(index[iL])

   if keyword_set(MAG_SOURCE) then begin
      source_flux_L_W=10^(-0.4*source_flux_L)*phi0_L
      print,'source_flux_L_W = ',source_flux_L_W
      ;source_flux_L_W=jy2w(tab_lambda_L,source_flux_L) ;source flux in W.m-2.m
      ;phi0_L_interp=interpol(phi0_Jy,lambda_phi0,tab_lambda_L)
      ;phi0_L_Jy_interp=interpol(phi0_Jy,lambda_phi0,tab_lambda_L)
      ;m_L = -2.5*(alog10(source_flux_L/phi0_L_Jy_interp)) ;magnitude in L band
   endif else begin
      source_flux_L_W=jy2w(tab_lambda_L,source_flux_L) ;source flux in W.m-2.m
      print,'source_flux_L_W = ',source_flux_L_W
      phi0_L_interp=interpol(phi0_Jy,lambda_phi0,tab_lambda_L)
      phi0_L_Jy_interp=interpol(phi0_Jy,lambda_phi0,tab_lambda_L)
      m_L = -2.5*(alog10(source_flux_L/phi0_L_Jy_interp)) ;magnitude in L band
      print,'m_L = ',m_L
   endelse

   nw_L=n_elements(tab_lambda_L)

if keyword_set(FIXED_WINDOW) then begin

if keyword_set(NOISE_PER_PIX) then begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral column
      n_pix_I_L=n_pix_I_32um ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_I_L    ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral column
      n_pix_I_L=n_pix_I_32um ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_P_32um ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endelse
endif else begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_L=n_pix_I_32um*(tab_lambda_L/(lambda_ref*1e-6)) ;nb of pixels read per spectral channel at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_I_L                                       ;nb of pixels read per spectral channel at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_L=n_pix_I_32um*(tab_lambda_L/(lambda_ref*1e-6)) ;nb of pixels read per spectral channel at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_P_32um*(tab_lambda_L/(lambda_ref*1e-6)) ;nb of pixels read per spectral channel at a given wavelength in a photometric channel
   endelse
endelse

endif else begin

if keyword_set(NOISE_PER_PIX) then begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_L=n_pix_I_32um*(tab_lambda_L/(lambda_ref*1e-6)) ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_I_L                                     ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_L=n_pix_I_32um*(tab_lambda_L/(lambda_ref*1e-6)) ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_I_L/6.                                  ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endelse
endif else begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_L=n_pix_I_32um*(tab_lambda_L/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_I_L                                       ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_L=n_pix_I_32um*(tab_lambda_L/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_L=n_pix_I_L/6.                                    ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endelse
endelse

endelse

print,'Number of pixels read per spectral column (interf) = ',n_pix_I_L
print,'Number of pixels read per spectral column (phot) = ',n_pix_P_L



   if telescope eq 0 then begin ;we choose AT
    
      ;Instrumental visibility
      if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Vinst_L=Vinst_L_AT[0] else Vinst_L=Vinst_L_AT[1]
     
      ;number of photons from the source
      if keyword_set(add_trans) then begin
         a=execute('resolution=R_'+Res_LM)
         readcol,Ta_LM,lam,trans_atm
         n_smooth=fix(Res_spec/resolution)
         trans_atm=smooth(trans_atm,n_smooth)
         trans_atm_L=interpol(trans_atm,lam,tab_lambda_L*1e+6,/quadratic)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Trans_L_AT=trans_source(SR_L,trans_atm_L,TI_L_AT,TW_L,TC_L[0],TP_L_AT,TS_L_AT)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then Trans_L_AT=trans_source(SR_L,trans_atm_L,TI_L_AT,TW_L,TC_L[1],TP_L_AT,TS_L_AT)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then Trans_L_AT=trans_source(SR_L,trans_atm_L,TI_L_AT,TW_L,TC_L[2],TP_L_AT,TS_L_AT)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then Trans_L_AT=trans_source(SR_L,trans_atm_L,TI_L_AT,TW_L,TC_L[3],TP_L_AT,TS_L_AT)
         nphoton_L_AT = Trans_L_AT*S_tel_AT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nphoton_L_AT = Trans_L_AT[0]*S_tel_AT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nphoton_L_AT = Trans_L_AT[1]*S_tel_AT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then nphoton_L_AT = Trans_L_AT[2]*S_tel_AT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then nphoton_L_AT = Trans_L_AT[3]*S_tel_AT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
      endelse

      ;number of photon of the background
      if keyword_set(add_trans) then begin
         Eps_L_AT=emiss_bg(trans_atm_L,TI_L_AT,TW_L,Cp_L_AT)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[0]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[1]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[2]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[3]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[0]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[1]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[2]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then nbackground_L_AT = Transb_L[3]*Eps_L_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
      endelse

      print,'nb photons background = ',nbackground_L_AT
    
      ;Split of the photon numbers in the interferometric and photometric channels    
      nP_L_AT = nphoton_L_AT*alpha_P ;photon number in L band, for AT, in the photometric channel
      nPb_L_AT = nbackground_L_AT*alpha_P ;background photon number in L band, for AT, in the photometric channel
      nI_L_AT = nphoton_L_AT*alpha_I      ;photon number in L band, for AT, in the interferometric channel
      nIb_L_AT = nbackground_L_AT*alpha_I ;background photon number in L band, for AT, in the interferometric channel 

      ;Photometry SNR
      SNRn_L_AT = (nP_L_AT)/sqrt(2*nPb_L_AT+nP_L_AT+2*n_pix_P_L*(RON_LM^2));+struc_function(nPb_L_AT, t_chop))
      SNRn_L_AT_t = sqrt(R_chop[0]*t_obs/texp_LM_n)*(nP_L_AT)/sqrt(2*nPb_L_AT+nP_L_AT+2*n_pix_P_L*(RON_LM^2));+struc_function(nPb_L_AT, t_chop))

      ;Coherent flux SNR
      SNRc_L_AT=fltarr(nw_L)
      SNRc_L_AT_t=fltarr(nw_L)
      SNRv_L_AT=fltarr(nw_L)
       vis_L=vis(index[iL])
       
       SNRc_L_AT = (nI_L_AT*vis_L*Vinst_L)/sqrt(n_T*nIb_L_AT+n_T*nI_L_AT+n_pix_I_L*(RON_LM^2)) 
       SNRc_L_AT_t = sqrt(R_chop[0]*t_obs/texp_LM_c)*(nI_L_AT*vis_L*Vinst_L)/sqrt(n_T*nIb_L_AT+n_T*nI_L_AT+n_pix_I_L*(RON_LM^2));+(nI_L_AT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_L_AT(i)+nI_L_AT(i)))
       SNRv_L_AT = 1./(sqrt((1./SNRc_L_AT_t)^2+(1./SNRn_L_AT_t)^2))

       ;Here estimation of the absolute error on the coherent flux;

       print,'                                                                  '
       print,'*************************************************************************'
       print,'************************* L BAND ****************************************'
       print,'*************************************************************************'
       ;print,'Magnitude of the source: L=',m_L
       print,'SNR_C in L band with ATs (per frame) = ',SNRc_L_AT
 
       ;Relative error on the visibility
       SNRv_L_AT_pourcent = (1./SNRv_L_AT)*100
    
       ;Relative error and SNR on the calibrated visibility (fundamental and calibrated terms)
       error_v_fund=fltarr(nw_L,/nozero)
       error_v_cal=fltarr(nw_L,/nozero)
       error_v_fund = SNRv_L_AT_pourcent ;Relative error on V in % (fundamental noise)
       error_v_cal = sqrt(pine_L_AT_pourcent^2+strehl_L_AT_pourcent^2+OPD_jitter_L_AT_pourcent^2) ;Relative error on V in % (calibration)
       error_v=sqrt(error_v_fund^2+error_v_cal^2) ;Total relative error on V in %
       SNRv = (1./error_v)*100
       SNRv_fund = (1./error_v_fund)*100

       ;Squared coherent flux variance
       VARc2_L_AT=fltarr(nw_L)
       VARc2_L_AT=nI_L_AT^2*(vis_L*Vinst_L)^2*(2.*n_T*(nIb_L_AT+nI_L_AT)+2.*n_pix_I_L*RON_LM^2+4.)+n_T^2*(nIb_L_AT+nI_L_AT)^2+n_T*(nIb_L_AT+nI_L_AT)*(1.+2.*n_pix_I_L*RON_LM^2)+(3.+n_pix_I_L)*n_pix_I_L*RON_LM^4. 
       SNRc2_L_AT=(nI_L_AT*vis_L*Vinst_L)^2/sqrt(VARc2_L_AT)
       SNRc2_L_AT_t=sqrt(R_chop[0]*t_obs/texp_LM_c)*SNRc2_L_AT
       print,'n_frame = ',R_chop[0]*t_obs/texp_LM_c
       SNRc2_fund=SNRc2_L_AT_t
       
       ;Photometry variance
       VARn_L_AT=fltarr(nw_L)
       VARn_L_AT=2*nPb_L_AT+nP_L_AT+2*n_pix_P_L*(RON_LM^2);+struc_function(nPb_L_AT, t_chop)
       SNRn_L_AT=nP_L_AT/sqrt(VARn_L_AT)
       SNRn_L_AT_t=sqrt(R_chop[0]*t_obs/texp_LM_n)*SNRn_L_AT
       SNRn_fund=SNRn_L_AT_t

       ;Relative error on squared visibility
       error_rel_v2_L_AT=sqrt((1./SNRc2_L_AT)^2+2.*(1./SNRn_L_AT^2))
       error_rel_v2_L_AT_t=sqrt((1./SNRc2_L_AT_t)^2+2.*(1./SNRn_L_AT_t^2))

       ;SNR on squared visibility
       SNRv2_L_AT=1./error_rel_v2_L_AT
       SNRv2_L_AT_t=1./error_rel_v2_L_AT_t

       ;Relative error and SNR on the calibrated squared visibility (fundamental and calibrated terms)
       error_v2_fund=fltarr(nw_L,/nozero)
       error_v2_cal=fltarr(nw_L,/nozero)
       error_v2_fund = error_rel_v2_L_AT_t*100. ;Relative error on V in % (fundamental noise)
       error_v2_cal = sqrt(pine_L_AT_pourcent^2+strehl_L_AT_pourcent^2+OPD_jitter_L_AT_pourcent^2) ;Relative error on V in % (calibration)
       error_v2=sqrt(error_v2_fund^2+error_v2_cal^2) ;Total relative error on V in %
       SNRv2 = (1./error_v2)*100
       SNRv2_fund = (1./error_v2_fund)*100


       ;Absolute error on the differential phase (in rad)
       error_phi_fund=fltarr(nw_L)
       error_phi_cal=fltarr(nw_L)
       error_phi=fltarr(nw_L)

       error_phi_cal=sqrt(pine_L_phi_AT^2+strehl_L_phi_AT^2+opd_jitter_L_phi_AT^2+chro_L_phi_AT^2) ;additive error term taking into account instrumental and atmospheric errors
       ;error_phi_cal=sqrt(chro_L_phi_AT^2) ;additive error term taking into account instrumental and atmospheric errors
       error_phi_fund = sqrt((texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_L_AT^2)) ;error due to fundamental noises
       error_phi=sqrt(error_phi_fund^2+error_phi_cal^2) ;Total absolute error (in rad)


       ;Error on the closure phase (in rad)
       error_c_fund=fltarr(nw_L)
       error_c_cal=fltarr(nw_L)
       error_c=fltarr(nw_L)

       ;to be checked
       error_c_cal=sqrt(pine_L_clo_AT^2+strehl_L_clo_AT^2+det_L_clo_AT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_cal_1=sqrt(det_L_clo_AT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_fund = sqrt((3.*texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_L_AT^2))  ;Absolute error due to fundamental noises (rad)
       error_c=sqrt(error_c_fund^2+error_c_cal^2)
       error_c_1=sqrt(error_c_fund^2+error_c_cal_1^2)

       openw,1,'errv_vs_flux_L_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],error_v2_fund[i]/100.,(error_v2_fund[i]/100.)/(2.*vis_L[i]*Vinst_L)
       close,1

       openw,1,'errcorr_ratio_vs_flux_L_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],sqrt(2)*(1./SNRc2_L_AT_t[i])/2.,sqrt(2)*(1./SNRc_L_AT_t[i])
       close,1

       openw,1,'errcorr_vs_flux_L_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],tab_lambda_L[i],(source_flux_L[i]*Vinst_L*vis_L[i])^2/SNRc2_L_AT_t[i],((source_flux_L[i]*Vinst_L*vis_L[i])^2/SNRc2_L_AT_t[i])*1./(2.*vis_L[i]*Vinst_L*source_flux_L[i])
       close,1

       openw,1,'errcorr_rel_vs_flux_L_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],1./SNRc2_L_AT_t[i],1./(SNRc2_L_AT_t[i]*2.)
       close,1

       openw,1,'errphi_vs_flux_L_AT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_L[i],error_phi_fund[i]*180./!pi
       close,1

       openw,1,'errc_vs_flux_L_AT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_L[i],error_c_1[i]*180./!pi
       close,1
 

       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,'SNR and error estimations (with ATs) in L band'
       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,' wavelength = ',tab_lambda_L
       tab_lambda_final=tab_lambda_L
       print,' nb_photon_source = ',nphoton_L_AT
       print,' nb_photon_background = ',nbackground_L_AT
       ;print,'detector noise (photometry channel) = ',n_pix_P_L*(RON_LM^2)
       print,'detector noise (interferometry channel) = ',n_pix_I_L*(RON_LM^2)
       ;print,'Noise-to-signal-ratio (readout noise) = ',sqrt(n_pix_I_L*(RON_LM^2))/nphoton_L_AT
       ;print,'Total noise (interferometry channel) = ',n_T*nIb_L_AT+n_T*nI_L_AT+n_pix_I_L*(RON_LM^2)
       print,'----------------------------------------------------------------------------'
       print,'Object visibility  = ',vis_L
       print,'SNR on the non-calibrated visibility  = ',SNRv_fund
       print,'SNR on the calibrated visibility  = ',SNRv
       print,'SNR on the non-calibrated squared visibility  = ',SNRv2_fund
       print,'SNR on the calibrated squared visibility  = ',SNRv2
       print,'Relative error on the fundamental visibility [%] = ',error_v_fund
       print,'Relative error on the calibrated visibility [%] = ',error_v
       print,'Relative error on the fundamental squared visibility [%] = ',error_v2_fund
       print,'Relative error on the calibrated squared visibility [%] = ',error_v2
       print,'Total error on the differential phase (rad) = ',error_phi
       print,'fundamental error on the differential phase (rad) = ',error_phi_fund
       print,'SNR on the coherent flux = ',1./(sqrt(2)*error_phi_fund)
       print,'Total error on the closure phase (rad )= ',error_c
       print,'fundamental error on the closure phase (rad)= ',error_c_fund
       print,'----------------------------------------------------------------------------'
       print,'                                                                            '

    endif

   if telescope eq 1 then begin ;we choose UT
         
      ;Instrumental visibility
      if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Vinst_L=Vinst_L_UT[0] else Vinst_L=Vinst_L_UT[1]
 
      ;number of photons from the source
      if keyword_set(add_trans) then begin
         a=execute('resolution=R_'+Res_LM)
         readcol,Ta_LM,lam,trans_atm
         n_smooth=fix(Res_spec/resolution)
         trans_atm=smooth(trans_atm,n_smooth)
         trans_atm_L=interpol(trans_atm,lam,tab_lambda_L*1e+6,/quadratic)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Trans_L_UT=trans_source(SR_L,trans_atm_L,TI_L_UT,TW_L,TC_L[0],TP_L_UT,TS_L_UT)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then Trans_L_UT=trans_source(SR_L,trans_atm_L,TI_L_UT,TW_L,TC_L[1],TP_L_UT,TS_L_UT)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then Trans_L_UT=trans_source(SR_L,trans_atm_L,TI_L_UT,TW_L,TC_L[2],TP_L_UT,TS_L_UT)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then Trans_L_UT=trans_source(SR_L,trans_atm_L,TI_L_UT,TW_L,TC_L[3],TP_L_UT,TS_L_UT)
         nphoton_L_UT = Trans_L_UT*S_tel_UT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nphoton_L_UT = Trans_L_UT[0]*S_tel_UT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nphoton_L_UT = Trans_L_UT[1]*S_tel_UT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then nphoton_L_UT = Trans_L_UT[2]*S_tel_UT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then nphoton_L_UT = Trans_L_UT[3]*S_tel_UT*delta_lambda_L*DIT*source_flux_L_W*tab_lambda_L/(h_planck*c)
      endelse

      ;number of photon of the background
      if keyword_set(add_trans) then begin
         Eps_L_UT=emiss_bg(trans_atm_L,TI_L_UT,TW_L,Cp_L_UT)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[0]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[1]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[2]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[3]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[0]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[1]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_high',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[2]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
         if strcmp(Res_LM,'L_veryhigh',/FOLD_CASE) eq 1 then nbackground_L_UT = Transb_L[3]*Eps_L_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_L*DIT*blackbody(tab_lambda_L,Temp)*tab_lambda_L/(h_planck*c)
      endelse

    
      ;Split of the photon numbers in the interferometric and photometric channels    
      nP_L_UT = nphoton_L_UT*alpha_P ;photon number in L band, for UT, in the photometric channel
      nPb_L_UT = nbackground_L_UT*alpha_P ;background photon number in L band, for UT, in the photometric channel
      nI_L_UT = nphoton_L_UT*alpha_I      ;photon number in L band, for UT, in the interferometric channel
      nIb_L_UT = nbackground_L_UT*alpha_I ;background photon number in L band, for UT, in the interferometric channel 

      ;Photometry SNR
      SNRn_L_UT = (nP_L_UT)/sqrt(2*nPb_L_UT+nP_L_UT+2*n_pix_P_L*(RON_LM^2)+struc_function(nPb_L_UT, t_chop))
      SNRn_L_UT_t = sqrt(R_chop[0]*t_obs/texp_LM_n)*(nP_L_UT)/sqrt(2*nPb_L_UT+nP_L_UT+2*n_pix_P_L*(RON_LM^2)+struc_function(nPb_L_UT, t_chop))

      ;Coherent flux SNR
      SNRc_L_UT=fltarr(nw_L)
      SNRc_L_UT_t=fltarr(nw_L)
      SNRv_L_UT=fltarr(nw_L)
      vis_L=vis(index[iL])
      SNRc_L_UT = (nI_L_UT*vis_L*Vinst_L)/sqrt(n_T*nIb_L_UT+n_T*nI_L_UT+n_pix_I_L*(RON_LM^2)) 
      SNRc_L_UT_t = sqrt(R_chop[0]*t_obs/texp_LM_c)*(nI_L_UT*vis_L*Vinst_L)/sqrt(n_T*nIb_L_UT+n_T*nI_L_UT+n_pix_I_L*(RON_LM^2));+(nI_L_UT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_L_UT(i)+nI_L_UT(i)))
      SNRv_L_UT = 1./(sqrt((1./SNRc_L_UT_t)^2+(1./SNRn_L_UT_t)^2))

      print,'                                                                         '
      print,'*************************************************************************'
      print,'************************* L BAND ****************************************'
      print,'*************************************************************************'
      print,'Magnitude of the source: L=',m_L
      print,'Signal (C) = ',(nI_L_UT*vis_L*Vinst_L)
      print,'nI = ',nI_L_UT
      print,'vis_L = ',vis_L
      print,'Vinst_L = ',Vinst_L
      print,'Var_C in L band with UTs (per frame) = ',n_T*nIb_L_UT+n_T*nI_L_UT+n_pix_I_L*(RON_LM^2)
      print,'SNR_C in L band with UTs (per frame) = ',SNRc_L_UT
 
       ;Relative error on the visibility
       SNRv_L_UT_pourcent = (1./SNRv_L_UT)*100
    
       ;Relative error and SNR on the calibrated visibility and squared visibility (fundamental and calibrated terms)
       error_v_fund=fltarr(nw_L,/nozero)
       ;error_v2_fund=fltarr(nw,/nozero)
       error_v_cal=fltarr(nw_L,/nozero)
       ;error_v2_cal=fltarr(nw,/nozero)
       error_v_fund = SNRv_L_UT_pourcent ;Relative error on V in % (fundamental noise)
       error_v_cal = sqrt(pine_L_UT_pourcent^2+strehl_L_UT_pourcent^2+OPD_jitter_L_UT_pourcent^2) ;Relative error on V in % (calibration)
       error_v=sqrt(error_v_fund^2+error_v_cal^2) ;Total relative error on V in %
       ;error_v2=sqrt(error_v2_fund^2+error_v2_cal^2)  ;Total absolute error on V2
       SNRv = (1./error_v)*100
       SNRv_fund = (1./error_v_fund)*100


       ;Squared coherent flux variance
       VARc2_L_UT=fltarr(nw_L)
       VARc2_L_UT=nI_L_UT^2*(vis_L*Vinst_L)^2*(2.*n_T*(nIb_L_UT+nI_L_UT)+2.*n_pix_I_L*RON_LM^2+4.)+n_T^2*(nIb_L_UT+nI_L_UT)^2+n_T*(nIb_L_UT+nI_L_UT)*(1.+2.*n_pix_I_L*RON_LM^2)+(3.+n_pix_I_L)*n_pix_I_L*RON_LM^4. 
       SNRc2_L_UT=(nI_L_UT*vis_L*Vinst_L)^2/sqrt(VARc2_L_UT)
       SNRc2_L_UT_t=sqrt(R_chop[0]*t_obs/texp_LM_c)*SNRc2_L_UT
       SNRc2_fund=SNRc2_L_UT_t
       
       ;Photometry variance
       VARn_L_UT=fltarr(nw_L)
       VARn_L_UT=2*nPb_L_UT+nP_L_UT+2*n_pix_P_L*(RON_LM^2)+struc_function(nPb_L_UT, t_chop)
       SNRn_L_UT=nP_L_UT/sqrt(VARn_L_UT)
       print,'Signal (photometry) = ',nP_L_UT
       print,'Var_n in L band with UTs (per frame) = ',VARn_L_UT
       print,'SNR_n in L band with UTs (per frame) = ',SNRn_L_UT
       SNRn_L_UT_t=sqrt(R_chop[0]*t_obs/texp_LM_n)*SNRn_L_UT
       SNRn_fund=SNRn_L_UT_t

       ;Relative error on squared visibility
       error_rel_v2_L_UT=sqrt((1./SNRc2_L_UT)^2+2.*(1./SNRn_L_UT^2))
       error_rel_v2_L_UT_t=sqrt((1./SNRc2_L_UT_t)^2+2.*(1./SNRn_L_UT_t^2))

       ;SNR on squared visibility
       SNRv2_L_UT=1./error_rel_v2_L_UT
       SNRv2_L_UT_t=1./error_rel_v2_L_UT_t

       ;Relative error and SNR on the calibrated squared visibility (fundamental and calibrated terms)
       error_v2_fund=fltarr(nw_L,/nozero)
       error_v2_cal=fltarr(nw_L,/nozero)
       error_v2_fund = error_rel_v2_L_UT_t*100. ;Relative error on V in % (fundamental noise)
       error_v2_cal = sqrt(pine_L_UT_pourcent^2+strehl_L_UT_pourcent^2+OPD_jitter_L_UT_pourcent^2) ;Relative error on V in % (calibration)
       error_v2=sqrt(error_v2_fund^2+error_v2_cal^2) ;Total relative error on V in %
       SNRv2 = (1./error_v2)*100
       SNRv2_fund = (1./error_v2_fund)*100


       ;Absolute error on the differential phase (in rad)
       error_phi_fund=fltarr(nw_L)
       error_phi_cal=fltarr(nw_L)
       error_phi=fltarr(nw_L)

       error_phi_cal=sqrt(pine_L_phi_UT^2+strehl_L_phi_UT^2+opd_jitter_L_phi_UT^2+chro_L_phi_UT^2) ;additive error term taking into account instrumental and atmospheric errors
       error_phi_fund = sqrt((texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_L_UT^2)) ;error due to fundamental noises
       error_phi=sqrt(error_phi_fund^2+error_phi_cal^2) ;Total absolute error (in rad)


       ;Error on the closure phase (in rad)
       error_c_fund=fltarr(nw_L)
       error_c_cal=fltarr(nw_L)
       error_c=fltarr(nw_L)

       ;to be checked
       ;error_c_cal=sqrt(pine_L_clo_UT^2+strehl_L_clo_UT^2+det_L_clo_UT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_cal=sqrt(det_L_clo_UT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_fund = sqrt((3.*texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_L_UT^2))  ;Absolute error due to fundamental noises (rad)
       error_c=sqrt(error_c_fund^2+error_c_cal^2)
 

       openw,1,'errv_vs_flux_L_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],error_v2_fund[i]/100.,(error_v2_fund[i]/100.)/(2.*vis_L[i]*Vinst_L)
       close,1

       openw,1,'errcorr_ratio_vs_flux_L_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],sqrt(2)*(1./SNRc2_L_UT_t[i])/2.,sqrt(2)*(1./SNRc_L_UT_t[i])
       close,1

       openw,1,'errcorr_vs_flux_L_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],tab_lambda_L[i],(source_flux_L[i]*Vinst_L*vis_L[i])^2/SNRc2_L_UT_t[i],((source_flux_L[i]*Vinst_L*vis_L[i])^2/SNRc2_L_UT_t[i])*1./(2.*vis_L[i]*Vinst_L*source_flux_L[i])
       close,1

       openw,1,'errcorr_rel_vs_flux_L_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_L[i],1./SNRc2_L_UT_t[i],1./(SNRc2_L_UT_t[i]*2.)
       close,1

       openw,1,'errphi_vs_flux_L_UT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_L[i],error_phi_fund[i]*180./!pi
       close,1

       openw,1,'errc_vs_flux_L_UT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_L[i],error_c[i]*180./!pi
       close,1


       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,'SNR and error estimations (with UTs) in L band'
       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,' wavelength = ',tab_lambda_L
       tab_lambda_final=tab_lambda_L
       print,' nb_photon_source = ',nphoton_L_UT
       print,' nb_photon_background = ',nbackground_L_UT
       ;print,'detector noise (photometry channel) = ',n_pix_P_L*(RON_LM^2)
       ;print,'detector noise (interferometry channel) = ',n_pix_I_L*(RON_LM^2)
       ;print,'Noise-to-signal-ratio (readout noise) = ',sqrt(n_pix_I_L*(RON_LM^2))/nphoton_L_UT
       ;print,'Total noise (interferometry channel) = ',n_T*nIb_L_UT+n_T*nI_L_UT+n_pix_I_L*(RON_LM^2)
       print,'----------------------------------------------------------------------------'
       print,'Object visibility  = ',vis_L
       print,'SNR on the non-calibrated visibility  = ',SNRv_fund
       print,'SNR on the calibrated visibility  = ',SNRv
       print,'SNR on the non-calibrated squared visibility  = ',SNRv2_fund
       print,'SNR on the calibrated squared visibility  = ',SNRv2
       print,'Relative error on the fundamental visibility [%] = ',error_v_fund
       print,'Relative error on the calibrated visibility [%] = ',error_v
       print,'Relative error on the fundamental squared visibility [%] = ',error_v2_fund
       print,'Relative error on the calibrated squared visibility [%] = ',error_v2
       print,'Total error on the differential phase (rad) = ',error_phi
       print,'fundamental error on the differential phase (rad) = ',error_phi_fund
       print,'SNR on the coherent flux = ',1./(sqrt(2)*error_phi_fund)
       print,'Total error on the closure phase (rad )= ',error_c
       print,'fundamental error on the closure phase (rad)= ',error_c_fund
       print,'----------------------------------------------------------------------------'
       print,'                                                                            '

    endif

   print,'tab_lambda_final = ',tab_lambda_final

   set_plot,'ps'
   !p.multi=[0,2,2]
   Device,Decomposed=0,color=1,BITS_PER_PIXEL=8
   loadct,39
   device,filename='SNR_L.eps',/ENCAPSULATED
   plot,tab_lambda_final*1e+6,SNRc2_fund,thick=2,title='SNR on c2',xtitle='wavelength [um]',ytitle='SNR C2',xstyle=1,xr=[2.8,4.2]
   plot,tab_lambda_final*1e+6,SNRn_fund,thick=2,title='SNR on photometry',xtitle='wavelength [um]',ytitle='SNR photometry',xstyle=1,xr=[2.8,4.2]
   plot,tab_lambda_final*1e+6,SNRv2_fund,thick=2,title='SNR on V2',xtitle='wavelength [um]',ytitle='SNR V2',xstyle=1,xr=[2.8,4.2]
   plot,tab_lambda_final*1e+6,error_c_fund*180./!pi,thick=2,title='Closure phase error',xtitle='wavelength [um]',ytitle='Closure phase error [deg]',xstyle=1,xr=[2.8,4.2]
   device,/close

   ;set_plot,'ps'
   ;device,filename='SNRv2_fund_L.ps'
   ;plot,tab_lambda_final*1e+6,SNRv2_fund,thick=2,title='Fundamental SNR on V2',xtitle='wavelength [um]',ytitle='SNR V2',psym=4,xstyle=1,xr=[2.8,4.2]
   ;device,/close

   ;set_plot,'ps'
   ;device,filename='SNRc2_fund_L.ps'
   ;plot,tab_lambda_final*1e+6,SNRc2_fund,thick=2,title='Fundamental SNR on c2',xtitle='wavelength [um]',ytitle='SNR C2',psym=4,xstyle=1,xr=[2.8,4.2]
   ;device,/close

   ;set_plot,'ps'
   ;device,filename='SNRn_fund_L.ps'
   ;plot,tab_lambda_final*1e+6,SNRn_fund,thick=2,title='Fundamental SNR on photometry',xtitle='wavelength [um]',ytitle='SNR photometry',psym=4,xstyle=1,xr=[2.8,4.2]
   ;device,/close

   ;set_plot,'ps'
   ;device,filename='errort3_fund_L.ps'
   ;plot,tab_lambda_final*1e+6,error_c_fund*180./!pi,thick=2,title='Closure phase error',xtitle='wavelength [um]',ytitle='Closure phase error [deg]',psym=4,xstyle=1,xr=[2.8,4.2]
   ;device,/close

endif





;-----------------------------
;M band
;-----------------------------
ind=where(tab_lambda gt 4.4e-6,count1)
if (count1 ne 0) then begin
iM=where(tab_lambda[ind] lt 5.2e-6,count)
if count eq 0 then begin
   print,'you are out of the M band (4.5 um - 5.0 um)'
   stop
endif
if count ne 0 then begin
   tab_lambda_M=tab_lambda(ind[iM])
   delta_lambda_M=delta_lambda(ind[iM])
   source_flux_M=source_flux(ind[iM])

   if keyword_set(MAG_SOURCE) then begin      
      source_flux_M_W=10^(-0.4*source_flux_M)*phi0_M
   endif else begin
      source_flux_M_W=jy2w(tab_lambda_M,source_flux_M) ;source flux in W.m-2.m
      phi0_M_interp=interpol(phi0,lambda_phi0,tab_lambda_M)
      phi0_M_Jy_interp=interpol(phi0_Jy,lambda_phi0,tab_lambda_M)
      m_M = -2.5*(alog10(source_flux_M/phi0_M_Jy_interp)) ;magnitude in M band
      print,'m_M = ',m_M
   endelse
   
   nw_M=n_elements(tab_lambda_M)

if keyword_set(FIXED_WINDOW) then begin

if keyword_set(NOISE_PER_PIX) then begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral column
      n_pix_I_M=n_pix_I_32um ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_I_M    ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral column
      n_pix_I_M=n_pix_I_32um ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_P_32um ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endelse
endif else begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_M=n_pix_I_32um*(tab_lambda_M/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_I_M                                       ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_M=n_pix_I_32um*(tab_lambda_M/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_P_32um*(tab_lambda_M/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endelse
endelse

endif else begin

if keyword_set(NOISE_PER_PIX) then begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_M=n_pix_I_32um*(tab_lambda_M/(lambda_ref*1e-6)) ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_I_M                                     ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_M=n_pix_I_32um*(tab_lambda_M/(lambda_ref*1e-6)) ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_I_M/6.                                  ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endelse
endif else begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_M=n_pix_I_32um*(tab_lambda_M/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_I_M                                       ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_M=n_pix_I_32um*(tab_lambda_M/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_M=n_pix_I_M/6.                                    ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endelse
endelse

endelse


   if telescope eq 0 then begin ;we choose AT
    
      ;Instrumental visibility
      if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Vinst_M=Vinst_M_AT[0] else Vinst_M=Vinst_M_AT[1]


      ;number of photons from the source
      if keyword_set(add_trans) then begin
         a=execute('resolution=R_'+Res_LM)
         readcol,Ta_LM,lam,trans_atm
         n_smooth=fix(Res_spec/resolution)
         trans_atm=smooth(trans_atm,n_smooth)
         trans_atm_M=interpol(trans_atm,lam,tab_lambda_M*1e+6,/quadratic)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Trans_M_AT=trans_source(SR_M,trans_atm_M,TI_M_AT,TW_M,TC_M[0],TP_M_AT,TS_M_AT)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then Trans_M_AT=trans_source(SR_M,trans_atm_M,TI_M_AT,TW_M,TC_M[1],TP_M_AT,TS_M_AT)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then Trans_M_AT=trans_source(SR_M,trans_atm_M,TI_M_AT,TW_M,TC_M[2],TP_M_AT,TS_M_AT) 
         nphoton_M_AT = Trans_M_AT*S_tel_AT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nphoton_M_AT = Trans_M_AT[0]*S_tel_AT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nphoton_M_AT = Trans_M_AT[1]*S_tel_AT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then nphoton_M_AT = Trans_M_AT[2]*S_tel_AT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
      endelse

      ;number of photon of the background
      if keyword_set(add_trans) then begin
         Eps_M_AT=emiss_bg(trans_atm_M,TI_M_AT,TW_M,Cp_M_AT)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_M_AT = Transb_M[0]*Eps_M_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_M_AT = Transb_M[1]*Eps_M_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then nbackground_M_AT = Transb_M[2]*Eps_M_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_M_AT = Transb_M[0]*Eps_M_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_M_AT = Transb_M[1]*Eps_M_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then nbackground_M_AT = Transb_M[2]*Eps_M_AT*S_tel_AT*delta_Omega_LM_AT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
      endelse

      print,'nb photons background = ',nbackground_M_AT
    
      ;Split of the photon numbers in the interferometric and photometric channels    
      nP_M_AT = nphoton_M_AT*alpha_P ;photon number in M band, for AT, in the photometric channel
      nPb_M_AT = nbackground_M_AT*alpha_P ;background photon number in M band, for AT, in the photometric channel
      nI_M_AT = nphoton_M_AT*alpha_I      ;photon number in M band, for AT, in the interferometric channel
      nIb_M_AT = nbackground_M_AT*alpha_I ;background photon number in M band, for AT, in the interferometric channel 

      ;Photometry SNR
      SNRn_M_AT = (nP_M_AT)/sqrt(2*nPb_M_AT+nP_M_AT+2*n_pix_P_M*(RON_LM^2)+struc_function(nPb_M_AT, t_chop))
      SNRn_M_AT_t = sqrt(R_chop[0]*t_obs/texp_LM_n)*(nP_M_AT)/sqrt(2*nPb_M_AT+nP_M_AT+2*n_pix_P_M*(RON_LM^2)+struc_function(nPb_M_AT, t_chop))

      ;Coherent flux SNR
      SNRc_M_AT=fltarr(nw_M)
      SNRc_M_AT_t=fltarr(nw_M)
      SNRv_M_AT=fltarr(nw_M)
       vis_M=vis(ind[iM])
       SNRc_M_AT = (nI_M_AT*vis_M*Vinst_M)/sqrt(n_T*nIb_M_AT+n_T*nI_M_AT+n_pix_I_M*(RON_LM^2)) 
       SNRc_M_AT_t = sqrt(R_chop[0]*t_obs/texp_LM_c)*(nI_M_AT*vis_M*Vinst_M)/sqrt(n_T*nIb_M_AT+n_T*nI_M_AT+n_pix_I_M*(RON_LM^2));+(nI_L_AT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_L_AT(i)+nI_L_AT(i)))
       SNRv_M_AT = 1./(sqrt((1./SNRc_M_AT_t)^2+(1./SNRn_M_AT_t)^2))

       print,'                                                                  '
       print,'*************************************************************************'
       print,'************************* M BAND ****************************************'
       print,'*************************************************************************'
       print,'SNR_C in M band with ATs (per frame) = ',SNRc_M_AT
       print,'Struct_function = ',struc_function(nPb_M_AT, t_chop)
 
       ;Relative error on the visibility
       SNRv_M_AT_pourcent = (1./SNRv_M_AT)*100
    
       ;Relative error and SNR on the calibrated visibility and squared visibility (fundamental and calibrated terms)
       error_v_fund=fltarr(nw_M,/nozero)
       error_v_cal=fltarr(nw_M,/nozero)
       error_v_fund = SNRv_M_AT_pourcent ;Relative error on V in % (fundamental noise)
       error_v_cal = sqrt(pine_M_AT_pourcent^2+strehl_M_AT_pourcent^2+OPD_jitter_M_AT_pourcent^2) ;Relative error on V in % (calibration)
       error_v=sqrt(error_v_fund^2+error_v_cal^2) ;Total relative error on V in %
       SNRv = (1./error_v)*100
       SNRv_fund = (1./error_v_fund)*100


       ;Squared coherent flux variance
       VARc2_M_AT=fltarr(nw_M)
       VARc2_M_AT=nI_M_AT^2*(vis_M*Vinst_M)^2*(2.*n_T*(nIb_M_AT+nI_M_AT)+2.*n_pix_I_M*RON_LM^2+4.)+n_T^2*(nIb_M_AT+nI_M_AT)^2+n_T*(nIb_M_AT+nI_M_AT)*(1.+2.*n_pix_I_M*RON_LM^2)+(3.+n_pix_I_M)*n_pix_I_M*RON_LM^4. 
       SNRc2_M_AT=(nI_M_AT*vis_M*Vinst_M)^2/sqrt(VARc2_M_AT)
       SNRc2_M_AT_t=sqrt(R_chop[0]*t_obs/texp_LM_c)*SNRc2_M_AT
       SNRc2_fund=SNRc2_M_AT_t
       
       ;Photometry variance
       VARn_M_AT=fltarr(nw_M)
       VARn_M_AT=2*nPb_M_AT+nP_M_AT+2*n_pix_P_M*(RON_LM^2)+struc_function(nPb_M_AT, t_chop)
       SNRn_M_AT=nP_M_AT/sqrt(VARn_M_AT)
       SNRn_M_AT_t=sqrt(R_chop[0]*t_obs/texp_LM_n)*SNRn_M_AT
       SNRn_fund=SNRn_M_AT_t

       ;Relative error on squared visibility
       error_rel_v2_M_AT=sqrt((1./SNRc2_M_AT)^2+2.*(1./SNRn_M_AT^2))
       error_rel_v2_M_AT_t=sqrt((1./SNRc2_M_AT_t)^2+2.*(1./SNRn_M_AT_t^2))

       ;SNR on squared visibility
       SNRv2_M_AT=1./error_rel_v2_M_AT
       SNRv2_M_AT_t=1./error_rel_v2_M_AT_t

       ;Relative error and SNR on the calibrated squared visibility (fundamental and calibrated terms)
       error_v2_fund=fltarr(nw_M,/nozero)
       error_v2_cal=fltarr(nw_M,/nozero)
       error_v2_fund = error_rel_v2_M_AT_t*100. ;Relative error on V in % (fundamental noise)
       error_v2_cal = sqrt(pine_M_AT_pourcent^2+strehl_M_AT_pourcent^2+OPD_jitter_M_AT_pourcent^2) ;Relative error on V in % (calibration)
       error_v2=sqrt(error_v2_fund^2+error_v2_cal^2) ;Total relative error on V in %
       SNRv2 = (1./error_v2)*100
       SNRv2_fund = (1./error_v2_fund)*100



       ;Absolute error on the differential phase (in rad)
       error_phi_fund=fltarr(nw_M)
       error_phi_cal=fltarr(nw_M)
       error_phi=fltarr(nw_M)

       error_phi_cal=sqrt(pine_M_phi_AT^2+strehl_M_phi_AT^2+opd_jitter_M_phi_AT^2+chro_M_phi_AT^2) ;additive error term taking into account instrumental and atmospheric errors
       error_phi_fund = sqrt((texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_M_AT^2)) ;error due to fundamental noises
       error_phi=sqrt(error_phi_fund^2+error_phi_cal^2) ;Total absolute error (in rad)


       ;Error on the closure phase (in rad)
       error_c_fund=fltarr(nw_M)
       error_c_cal=fltarr(nw_M)
       error_c=fltarr(nw_M)

       ;to be checked
       ;error_c_cal=sqrt(pine_M_clo_AT^2+strehl_M_clo_AT^2+det_M_clo_AT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       ;error_c_fund = sqrt((3.*texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_M_AT^2))  ;Absolute error due to fundamental noises (rad)
       ;error_c=sqrt(error_c_fund^2+error_c_cal^2)
       error_c_cal=sqrt(pine_M_clo_AT^2+strehl_M_clo_AT^2+det_M_clo_AT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_cal_1=sqrt(det_M_clo_AT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_fund = sqrt((3.*texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_M_AT^2))  ;Absolute error due to fundamental noises (rad)
       error_c=sqrt(error_c_fund^2+error_c_cal^2)
       error_c_1=sqrt(error_c_fund^2+error_c_cal_1^2)
 
        openw,1,'errv_vs_flux_M_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],error_v2_fund[i]/100.,(error_v2_fund[i]/100.)/(2.*vis_M[i]*Vinst_M)
       close,1

       openw,1,'errcorr_ratio_vs_flux_M_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],sqrt(2)*(1./SNRc2_M_AT_t[i])/2.,sqrt(2)*(1./SNRc_M_AT_t[i])
       close,1

       openw,1,'errcorr_vs_flux_M_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],tab_lambda_M[i],(source_flux_M[i]*Vinst_M*vis_M[i])^2/SNRc2_M_AT_t[i],((source_flux_M[i]*Vinst_M*vis_M[i])^2/SNRc2_M_AT_t[i])*1./(2.*vis_M[i]*Vinst_M*source_flux_M[i])
       close,1

       openw,1,'errcorr_rel_vs_flux_M_AT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],1./SNRc2_M_AT_t[i],1./(SNRc2_M_AT_t[i]*2.)
       close,1

       openw,1,'errphi_vs_flux_M_AT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_M[i],error_phi_fund[i]*180./!pi
       close,1

       openw,1,'errc_vs_flux_M_AT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_M[i],error_c_1[i]*180./!pi
       close,1

       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,'SNR and error estimations (with ATs) in M band'
       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,' wavelength = ',tab_lambda_M
       tab_lambda_final=tab_lambda_M
       print,' nb_photon_source = ',nphoton_M_AT
       print,' nb_photon_background = ',nbackground_M_AT
       ;print,'detector noise (photometry channel) = ',n_pix_P_M*(RON_LM^2)
       print,'detector noise (interferometry channel) = ',n_pix_I_M*(RON_LM^2)
       ;print,'Noise-to-signal-ratio (readout noise) = ',sqrt(n_pix_I_M*(RON_LM^2))/nphoton_M_AT
       ;print,'Total noise (interferometry channel) = ',n_T*nIb_M_AT+n_T*nI_M_AT+n_pix_I_M*(RON_LM^2)
       print,'----------------------------------------------------------------------------'
       print,'Object visibility  = ',vis_M
       print,'SNR on the non-calibrated visibility  = ',SNRv_fund
       print,'SNR on the calibrated visibility  = ',SNRv
       print,'SNR on the non-calibrated visibility  = ',SNRv2_fund
       print,'SNR on the calibrated visibility  = ',SNRv2
       print,'Relative error on the fundamental visibility [%] = ',error_v_fund
       print,'Relative error on the calibrated visibility [%] = ',error_v
       print,'Relative error on the fundamental squared visibility [%] = ',error_v2_fund
       print,'Relative error on the calibrated squared visibility [%] = ',error_v2
       print,'Total error on the differential phase (rad) = ',error_phi
       print,'fundamental error on the differential phase (rad) = ',error_phi_fund
       print,'SNR on the coherent flux = ',1./(sqrt(2)*error_phi_fund)
       print,'Total error on the closure phase (rad )= ',error_c
       print,'fundamental error on the closure phase (rad)= ',error_c_fund
       print,'----------------------------------------------------------------------------'
       print,'                                                                            '

    endif

   if telescope eq 1 then begin ;we choose UT
         
      ;Instrumental visibility
      if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Vinst_M=Vinst_M_UT[0] else Vinst_M=Vinst_M_UT[1]
 

     ;number of photons from the source
      if keyword_set(add_trans) then begin
          a=execute('resolution=R_'+Res_LM)
         readcol,Ta_LM,lam,trans_atm
         n_smooth=fix(Res_spec/resolution)
         trans_atm=smooth(trans_atm,n_smooth)
         trans_atm_M=interpol(trans_atm,lam,tab_lambda_M*1e+6,/quadratic)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then Trans_M_UT=trans_source(SR_M,trans_atm_M,TI_M_UT,TW_M,TC_M[0],TP_M_UT,TS_M_UT)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then Trans_M_UT=trans_source(SR_M,trans_atm_M,TI_M_UT,TW_M,TC_M[1],TP_M_UT,TS_M_UT)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then Trans_M_UT=trans_source(SR_M,trans_atm_M,TI_M_UT,TW_M,TC_M[2],TP_M_UT,TS_M_UT) 
         nphoton_M_UT = Trans_M_UT*S_tel_UT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nphoton_M_UT = Trans_M_UT[0]*S_tel_UT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nphoton_M_UT = Trans_M_UT[1]*S_tel_UT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then nphoton_M_UT = Trans_M_UT[2]*S_tel_UT*delta_lambda_M*DIT*source_flux_M_W*tab_lambda_M/(h_planck*c)
      endelse

      ;number of photon of the background
      if keyword_set(add_trans) then begin
         Eps_M_UT=emiss_bg(trans_atm_M,TI_M_UT,TW_M,Cp_M_UT)
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_M_UT = Transb_M[0]*Eps_M_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_M_UT = Transb_M[1]*Eps_M_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then nbackground_M_UT = Transb_M[2]*Eps_M_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
      endif else begin
         if strcmp(Res_LM,'LM_low',/FOLD_CASE) eq 1 then nbackground_M_UT = Transb_M[0]*Eps_M_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'LM_med',/FOLD_CASE) eq 1 then nbackground_M_UT = Transb_M[1]*Eps_M_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
         if strcmp(Res_LM,'M_veryhigh',/FOLD_CASE) eq 1 then nbackground_M_UT = Transb_M[2]*Eps_M_UT*S_tel_UT*delta_Omega_LM_UT*delta_lambda_M*DIT*blackbody(tab_lambda_M,Temp)*tab_lambda_M/(h_planck*c)
      endelse

      print,'nb photons background = ',nbackground_M_UT
    
      ;Split of the photon numbers in the interferometric and photometric channels    
      nP_M_UT = nphoton_M_UT*alpha_P ;photon number in L band, for UT, in the photometric channel
      nPb_M_UT = nbackground_M_UT*alpha_P ;background photon number in L band, for UT, in the photometric channel
      nI_M_UT = nphoton_M_UT*alpha_I      ;photon number in L band, for UT, in the interferometric channel
      nIb_M_UT = nbackground_M_UT*alpha_I ;background photon number in L band, for UT, in the interferometric channel 

      ;Photometry SNR
      SNRn_M_UT = (nP_M_UT)/sqrt(2*nPb_M_UT+nP_M_UT+2*n_pix_P_M*(RON_LM^2)+struc_function(nPb_M_UT, t_chop))
      SNRn_M_UT_t = sqrt(R_chop[0]*t_obs/texp_LM_n)*(nP_M_UT)/sqrt(2*nPb_M_UT+nP_M_UT+2*n_pix_P_M*(RON_LM^2)+struc_function(nPb_M_UT, t_chop))

      ;Coherent flux SNR
      SNRc_M_UT=fltarr(nw_M)
      SNRc_M_UT_t=fltarr(nw_M)
      SNRv_M_UT=fltarr(nw_M)
       vis_M=vis(ind[iM])
       SNRc_M_UT = (nI_M_UT*vis_M*Vinst_M)/sqrt(n_T*nIb_M_UT+n_T*nI_M_UT+n_pix_I_M*(RON_LM^2)) 
       SNRc_M_UT_t = sqrt(R_chop[0]*t_obs/texp_LM_c)*(nI_M_UT*vis_M*Vinst_M)/sqrt(n_T*nIb_M_UT+n_T*nI_M_UT+n_pix_I_M*(RON_LM^2));+(nI_L_UT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_L_UT(i)+nI_L_UT(i)))
       SNRv_M_UT = 1./(sqrt((1./SNRc_M_UT_t)^2+(1./SNRn_M_UT_t)^2))

       print,'                                                                  '
       print,'*************************************************************************'
       print,'************************* M BAND ****************************************'
       print,'*************************************************************************'
       print,'SNR_C in M band with UTs (per frame) = ',SNRc_M_UT
 
       ;Relative error on the visibility
       SNRv_M_UT_pourcent = (1./SNRv_M_UT)*100
    
       ;Relative error and SNR on the calibrated visibility and squared visibility (fundamental and calibrated terms)
       error_v_fund=fltarr(nw_M,/nozero)
       error_v_cal=fltarr(nw_M,/nozero)
       error_v_fund = SNRv_M_UT_pourcent ;Relative error on V in % (fundamental noise)
       error_v_cal = sqrt(pine_M_UT_pourcent^2+strehl_M_UT_pourcent^2+OPD_jitter_M_UT_pourcent^2) ;Relative error on V in % (calibration)
       error_v=sqrt(error_v_fund^2+error_v_cal^2) ;Total relative error on V in %
       SNRv = (1./error_v)*100
       SNRv_fund = (1./error_v_fund)*100


       ;Squared coherent flux variance
       VARc2_M_UT=fltarr(nw_M)
       VARc2_M_UT=nI_M_UT^2*(vis_M*Vinst_M)^2*(2.*n_T*(nIb_M_UT+nI_M_UT)+2.*n_pix_I_M*RON_LM^2+4.)+n_T^2*(nIb_M_UT+nI_M_UT)^2+n_T*(nIb_M_UT+nI_M_UT)*(1.+2.*n_pix_I_M*RON_LM^2)+(3.+n_pix_I_M)*n_pix_I_M*RON_LM^4. 
       SNRc2_M_UT=(nP_M_UT*vis_M*Vinst_M)^2/sqrt(VARc2_M_UT)
       SNRc2_M_UT_t=sqrt(R_chop[0]*t_obs/texp_LM_c)*SNRc2_M_UT
       SNRc2_fund=SNRc2_M_UT_t
       
       ;Photometry variance
       VARn_M_UT=fltarr(nw_M)
       VARn_M_UT=2*nPb_M_UT+nP_M_UT+2*n_pix_P_M*(RON_LM^2)+struc_function(nPb_M_UT, t_chop)
       SNRn_M_UT=nP_M_UT/sqrt(VARn_M_UT)
       SNRn_M_UT_t=sqrt(R_chop[0]*t_obs/texp_LM_n)*SNRn_M_UT
       SNRn_fund=SNRn_M_UT_t

       ;Relative error on squared visibility
       error_rel_v2_M_UT=sqrt((1./SNRc2_M_UT)^2+2.*(1./SNRn_M_UT^2))
       error_rel_v2_M_UT_t=sqrt((1./SNRc2_M_UT_t)^2+2.*(1./SNRn_M_UT_t^2))

       ;SNR on squared visibility
       SNRv2_M_UT=1./error_rel_v2_M_UT
       SNRv2_M_UT_t=1./error_rel_v2_M_UT_t

       ;Relative error and SNR on the calibrated squared visibility (fundamental and calibrated terms)
       error_v2_fund=fltarr(nw_M,/nozero)
       error_v2_cal=fltarr(nw_M,/nozero)
       error_v2_fund = error_rel_v2_M_UT_t*100. ;Relative error on V in % (fundamental noise)
       error_v2_cal = sqrt(pine_M_UT_pourcent^2+strehl_M_UT_pourcent^2+OPD_jitter_M_UT_pourcent^2) ;Relative error on V in % (calibration)
       error_v2=sqrt(error_v2_fund^2+error_v2_cal^2) ;Total relative error on V in %
       SNRv2 = (1./error_v2)*100
       SNRv2_fund = (1./error_v2_fund)*100



       ;Absolute error on the differential phase (in rad)
       error_phi_fund=fltarr(nw_M)
       error_phi_cal=fltarr(nw_M)
       error_phi=fltarr(nw_M)

       error_phi_cal=sqrt(pine_M_phi_UT^2+strehl_M_phi_UT^2+opd_jitter_M_phi_UT^2+chro_M_phi_UT^2) ;additive error term taking into account instrumental and atmospheric errors
       error_phi_fund = sqrt((texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_M_UT^2)) ;error due to fundamental noises
       error_phi=sqrt(error_phi_fund^2+error_phi_cal^2) ;Total absolute error (in rad)


       ;Error on the closure phase (in rad)
       error_c_fund=fltarr(nw_M)
       error_c_cal=fltarr(nw_M)
       error_c=fltarr(nw_M)

       ;to be checked
       ;error_c_cal=sqrt(pine_M_clo_UT^2+strehl_M_clo_UT^2+det_M_clo_UT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       ;error_c_fund = sqrt((3.*texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_M_UT^2))  ;Absolute error due to fundamental noises (rad)
       ;error_c=sqrt(error_c_fund^2+error_c_cal^2)
       error_c_cal=sqrt(pine_M_clo_UT^2+strehl_M_clo_UT^2+det_M_clo_UT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_cal_1=sqrt(det_M_clo_UT^2) ;additive error term taking into account instrumental and atmospheric errors (rad)
       error_c_fund = sqrt((3.*texp_LM_phi/(2*R_chop[0]*t_obs))*(1./SNRc_M_UT^2))  ;Absolute error due to fundamental noises (rad)
       error_c=sqrt(error_c_fund^2+error_c_cal^2)
       error_c_1=sqrt(error_c_fund^2+error_c_cal_1^2)

       openw,1,'errv_vs_flux_M_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],error_v2_fund[i]/100.,(error_v2_fund[i]/100.)/(2.*vis_M[i]*Vinst_M)
       close,1

       openw,1,'errcorr_ratio_vs_flux_M_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],sqrt(2)*(1./SNRc2_M_UT_t[i])/2.,sqrt(2)*(1./SNRc_M_UT_t[i])
       close,1

       openw,1,'errcorr_vs_flux_M_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],tab_lambda_M[i],(source_flux_M[i]*Vinst_M*vis_M[i])^2/SNRc2_M_UT_t[i],((source_flux_M[i]*Vinst_M*vis_M[i])^2/SNRc2_M_UT_t[i])*1./(2.*vis_M[i]*Vinst_M*source_flux_M[i])
       close,1

       openw,1,'errcorr_rel_vs_flux_M_UT.dat'
       for i=0,n_elements(error_v)-1 do printf,1,source_flux_M[i],1./SNRc2_M_UT_t[i],1./(SNRc2_M_UT_t[i]*2.)
       close,1

       openw,1,'errphi_vs_flux_M_UT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_M[i],error_phi_fund[i]*180./!pi
       close,1

       openw,1,'errc_vs_flux_M_UT.dat'
       for i=0,n_elements(error_c)-1 do printf,1,source_flux_M[i],error_c_1[i]*180./!pi
       close,1

       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,'SNR and error estimations (with UTs) in M band'
       print,'----------------------------------------------------------------------------'
       print,'----------------------------------------------------------------------------'
       print,' wavelength = ',tab_lambda_M
       tab_lambda_final=tab_lambda_M
       print,' nb_photon_source = ',nphoton_M_UT
       print,' nb_photon_background = ',nbackground_M_UT
       ;print,'detector noise (photometry channel) = ',n_pix_P_M*(RON_LM^2)
       print,'detector noise (interferometry channel) = ',n_pix_I_M*(RON_LM^2)
       ;print,'Noise-to-signal-ratio (readout noise) = ',sqrt(n_pix_I_M*(RON_LM^2))/nphoton_M_UT
       ;print,'Total noise (interferometry channel) = ',n_T*nIb_M_UT+n_T*nI_M_UT+n_pix_I_M*(RON_LM^2)
       print,'----------------------------------------------------------------------------'
       print,'Object visibility  = ',vis_M
       print,'SNR on the non-calibrated visibility  = ',SNRv_fund
       print,'SNR on the calibrated visibility  = ',SNRv
       print,'SNR on the non-calibrated squared visibility  = ',SNRv2_fund
       print,'SNR on the calibrated squared visibility  = ',SNRv2
       print,'Relative error on the fundamental visibility [%] = ',error_v_fund
       print,'Relative error on the calibrated visibility [%] = ',error_v
       print,'Relative error on the fundamental squared visibility [%] = ',error_v2_fund
       print,'Relative error on the calibrated squared visibility [%] = ',error_v2
       print,'Total error on the differential phase (rad) = ',error_phi
       print,'fundamental error on the differential phase (rad) = ',error_phi_fund
       print,'SNR on the coherent flux = ',1./(sqrt(2)*error_phi_fund)
       print,'Total error on the closure phase (rad )= ',error_c
       print,'fundamental error on the closure phase (rad)= ',error_c_fund
       print,'----------------------------------------------------------------------------'
       print,'                                                                            '

    endif

endif

 print,'tab_lambda_final = ',tab_lambda_final

   set_plot,'ps'
   !p.multi=[0,2,2]
   Device,Decomposed=0,color=1,BITS_PER_PIXEL=8
   loadct,39
   device,filename='SNR_M.eps',/ENCAPSULATED
   plot,tab_lambda_final*1e+6,SNRc2_fund,thick=2,title='Fundamental SNR on c2',xtitle='wavelength [um]',ytitle='SNR C2',xstyle=1,xr=[4.5,5.0]
   plot,tab_lambda_final*1e+6,SNRn_fund,thick=2,title='Fundamental SNR on photometry',xtitle='wavelength [um]',ytitle='SNR photometry',xstyle=1,xr=[4.5,5.0]
   plot,tab_lambda_final*1e+6,SNRv2_fund,thick=2,title='Fundamental SNR on V2',xtitle='wavelength [um]',ytitle='SNR V2',xstyle=1,xr=[4.5,5.0]
   plot,tab_lambda_final*1e+6,error_c_fund*180./!pi,thick=2,title='Closure phase error',xtitle='wavelength [um]',ytitle='Closure phase error [deg]',xstyle=1,xr=[4.5,5.0]
   device,/close

   ;set_plot,'ps'
   ;device,filename='SNRv2_fund_M.ps'
   ;plot,tab_lambda_final*1e+6,SNRv2_fund,thick=2,title='Fundamental SNR on V2',xtitle='wavelength [um]',ytitle='SNR V2',psym=4,xstyle=1,xr=[4.5,5.0]
   ;device,/close

   ;set_plot,'ps'
   ;device,filename='SNRc2_fund_M.ps'
   ;plot,tab_lambda_final*1e+6,SNRc2_fund,thick=2,title='Fundamental SNR on c2',xtitle='wavelength [um]',ytitle='SNR C2',psym=4,xstyle=1,xr=[4.5,5.0]
   ;device,/close

   ;set_plot,'ps'
   ;device,filename='SNRn_fund_M.ps'
   ;plot,tab_lambda_final*1e+6,SNRn_fund,thick=2,title='Fundamental SNR on photometry',xtitle='wavelength [um]',ytitle='SNR photometry',psym=4,xstyle=1,xr=[4.5,5.0]
   ;device,/close

   ;set_plot,'ps'
   ;device,filename='errort3_fund_M.ps'
   ;plot,tab_lambda_final*1e+6,error_c_fund*180./!pi,thick=2,title='Closure phase error',xtitle='wavelength [um]',ytitle='Closure phase error [deg]',psym=4,xstyle=1,xr=[4.5,5.0]
   ;device,/close

endif




endif




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  
;SNR and errors calculation in N band
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   

if tab_lambda(0) gt 7.5e-6 then begin

iN=where(tab_lambda lt 13.5e-6,count)
if count eq 0 then begin
   print,'you are out of the N band (7.5 um - 13.5 um)'
   stop
endif

;-----------------------------------------------
;Interpolation over the correct wavelength array
;-----------------------------------------------

lambda_ref=8.0
s='tab_lambda_pix=disp_'+Res_N
b=Execute(s) ;Wavelength array in pixel
ind_N=where(tab_lambda_pix gt 7.9)
ind_N_N=where(tab_lambda_pix[ind_N] lt 13.05)
tab_lambda_pix=tab_lambda_pix[ind_N[ind_N_N]]
if keyword_set(NOISE_PER_PIX) then begin
nlambda_pix=n_elements(tab_lambda_pix)
delta_lambda_pix=make_array(nlambda_pix,/double)
tab_lambda_pix=tab_lambda_pix*1e-6
delta_lambda_pix[0]=abs(tab_lambda_pix[1]-tab_lambda_pix[0])
for i=1,nlambda_pix-1 do delta_lambda_pix[i]=abs(tab_lambda_pix[i]-tab_lambda_pix[i-1])
delta_lambda_N=interpol(delta_lambda_pix,tab_lambda_pix,tab_lambda)

   if keyword_set(TAB_LAM_MATISSE) then begin
      tab_lambda=tab_lambda_pix
      delta_lambda_N=delta_lambda_pix
      source_flux=replicate(source_flux[0],n_elements(tab_lambda))
   endif

;print,'delta_lambda = ',delta_lambda_N
endif else begin
tab_lambda_matisse=compute_lambda_matisse(reverse(tab_lambda_pix),lambda_ref,pinhole_size_LM)
;print,'tab_lambda_matisse = ',tab_lambda_matisse.eff_wave
;print,'delta_lambda_matisse = ',tab_lambda_matisse.eff_band
delta_lambda_N=interpol(tab_lambda_matisse.eff_band,tab_lambda_matisse.eff_wave,tab_lambda)

   if keyword_set(TAB_LAM_MATISSE) then begin
      tab_lambda=tab_lambda_matisse.eff_wave
      delta_lambda_N=tab_lambda_matisse.eff_band
      source_flux=replicate(source_flux[0],n_elements(tab_lambda))
   endif

;print,'delta_lambda = ',delta_lambda_N
endelse


if keyword_set(NOISE_PER_PIX) then begin
;------------------------------------------
;Number of pixels read per spectral column
;------------------------------------------

if keyword_set(FIXED_WINDOW) then begin
   n_pix_I_8um=468.            ;nb of pixels read per spectral column at 3.2um in the interferometric channel (fixed window size in the spatial direction)  
   n_pix_P_8um=78.            ;nb of pixels read per spectral column at 3.2um in a photometric channel (fixed window size in the spatial direction)  
endif else begin
   n_pix_I_8um=288.            ;nb of pixels read per spectral column at 3.2um in the interferometric channel (lambda-dependent window size in the spatial direction) 
endelse
endif else begin

;------------------------------------------
;Number of pixels read per spectral channel
;------------------------------------------
if keyword_set(FIXED_WINDOW) then begin
   n_pix_I_8um=2808.            ;nb of pixels read per spectral column at 3.2um in the interferometric channel (fixed window size in the spatial direction) 
   n_pix_P_8um=468.            ;nb of pixels read per spectral column at 3.2um in a photometric channel (fixed window size in the spatial direction)  
endif else begin
   n_pix_I_8um=1728.            ;nb of pixels read per spectral column at 3.2um in the interferometric channel (lambda-dependent window size in the spatial direction) 
endelse

endelse


;------------
;Solid angle
;------------
delta_Omega_N_AT = (!dpi/4.)*(pinhole_size_N*10.5e-6/(D_tel_AT))^2 ;it corresponds to the development of 1-cos(x/2) when x is very small
delta_Omega_N_UT = (!dpi/4.)*(pinhole_size_N*10.5e-6/(D_tel_UT))^2 ;it corresponds to the development of 1-cos(x/2) when x is very small

;-----------------------------
;Photometry
;-----------------------------
if keyword_set(TAB_LAM_MATISSE) then begin
   tab_lambda_N=tab_lambda
   source_flux_N=source_flux
   ;print,'tab_lambda_N = ',tab_lambda_N
   ;print,'source_flux_N = ',source_flux_N
endif else begin
   tab_lambda_N=tab_lambda[iN]
   source_flux_N=source_flux[iN]
endelse

if keyword_set(MAG_SOURCE) then begin
   source_flux_N_W=10^(-0.4*source_flux_N)*phi0_N
   print,'source_flux_N_W = ',source_flux_N_W
endif else begin
   source_flux_N_W=jy2w(tab_lambda_N,source_flux_N) ;source flux in W.m-2.m
   print,'source_flux_N_W = ',source_flux_N_W
   phi0_N_interp=interpol(phi0_Jy,lambda_phi0,tab_lambda_N)
   phi0_N_Jy_interp=interpol(phi0_Jy,lambda_phi0,tab_lambda_N)
   m_N = -2.5*(alog10(source_flux_N/phi0_N_Jy_interp)) ;magnitude in N band
   print,'m_N = ',m_N
endelse
nw_N=n_elements(tab_lambda_N)


;-----------------------------
;Number of pixels read
;-----------------------------

if keyword_set(FIXED_WINDOW) then begin

if keyword_set(NOISE_PER_PIX) then begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral column
      n_pix_I_N=n_pix_I_8um ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_I_N    ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral column
      n_pix_I_N=n_pix_I_8um ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_P_8um ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endelse
endif else begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_N=n_pix_I_8um*(tab_lambda_N/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_I_N                                       ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_N=n_pix_I_8um*(tab_lambda_N/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_P_8um*(tab_lambda_N/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endelse
endelse

endif else begin

if keyword_set(NOISE_PER_PIX) then begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_N=n_pix_I_8um*(tab_lambda_N/(lambda_ref*1e-6)) ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_I_N                                     ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_N=n_pix_I_8um*(tab_lambda_N/(lambda_ref*1e-6)) ;nb of pixels read per spectral column at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_I_N/6.                                  ;nb of pixels read per spectral column at at a given wavelength in a photometric channel
   endelse
endif else begin
   if mode eq 1 then begin ;HighSens mode
      ;Number of pixels read per spectral channel
      n_pix_I_N=n_pix_I_8um*(tab_lambda_N/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_I_N                                       ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endif else begin ;Siphot mode
      ;Number of pixels read per spectral channel
      n_pix_I_N=n_pix_I_8um*(tab_lambda_N/(lambda_ref*1e-6))^2 ;nb of pixels read per spectral channel at at a given wavelength in the interferometric channel
      n_pix_P_N=n_pix_I_N/6.                                    ;nb of pixels read per spectral channel at at a given wavelength in a photometric channel
   endelse
endelse

endelse


if telescope eq 0 then begin ;we choose AT
    
   ;Instrumental visibility
   if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then Vinst_N=Vinst_N_AT[0] else Vinst_N=Vinst_N_AT[1]
     
   ;number of photons from the source
   if keyword_set(add_trans) then begin
      a=execute('resolution=R_'+Res_N)
      readcol,Ta_N,lam,trans_atm
      n_smooth=fix(Res_spec/resolution)
      trans_atm=smooth(trans_atm,n_smooth)
      trans_atm_N=interpol(trans_atm,lam,tab_lambda_N*1e+6,/spline)
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then Trans_N_AT=trans_source(SR_N,trans_atm_N,TI_N_AT,TW_N,TC_N[0],TP_N_AT,TS_N_AT) else Trans_N_AT=trans_source(SR_N,trans_atm_N,TI_N_AT,TW_N,TC_N[1],TP_N_AT,TS_N_AT)
      nphoton_N_AT = Trans_N_AT*S_tel_AT*delta_lambda_N*DIT*source_flux_N_W*tab_lambda_N/(h_planck*c)
   endif else begin
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then nphoton_N_AT = Trans_N_AT[0]*S_tel_AT*delta_lambda_N*DIT*source_flux_N_W*tab_lambda_N/(h_planck*c) else nphoton_N_AT = Trans_N_AT[1]*S_tel_AT*delta_lambda_N*DIT*source_flux_N_W*tab_lambda_N/(h_planck*c)
   endelse
   print,'tab_lambda_N = ',tab_lambda_N
   print,'nb photons = ',nphoton_N_AT

   ;number of photon of the background
   if keyword_set(add_trans) then begin
      Eps_N_AT=emiss_bg(trans_atm_N,TI_N_AT,TW_N,Cp_N_AT)
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then begin
         nbackground_N_AT = Transb_N[0]*Eps_N_AT*S_tel_AT*delta_Omega_N_AT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c)
      endif else begin
         nbackground_N_AT = Transb_N[1]*Eps_N_AT*S_tel_AT*delta_Omega_N_AT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c)
      endelse

   endif else begin
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then nbackground_N_AT = Transb_N[0]*Eps_N_AT*S_tel_AT*delta_Omega_N_AT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c) else nbackground_N_AT = Transb_N[1]*Eps_N_AT*S_tel_AT*delta_Omega_N_AT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c)
   endelse

   print,'nb photons background = ',nbackground_N_AT/(QE_N*DIT)
    
   ;Split of the photon numbers in the interferometric and photometric channels    
   nP_N_AT = nphoton_N_AT*alpha_P         ;photon number in L band, for AT, in the photometric channel
   nPb_N_AT = nbackground_N_AT*alpha_P    ;background photon number in L band, for AT, in the photometric channel
   nI_N_AT = nphoton_N_AT*alpha_I         ;photon number in L band, for AT, in the interferometric channel
   nIb_N_AT = nbackground_N_AT*alpha_I    ;background photon number in L band, for AT, in the interferometric channel 

   ;Photometry SNR
   if mode eq 0 then begin ;Si_Phot mode
      SNRn_N_AT = (nP_N_AT)/sqrt(2*nPb_N_AT+nP_N_AT+2*n_pix_P_N*(RON_N^2));+struc_function(nPb_N_AT, t_chop))
      SNRn_N_AT_t = sqrt(R_chop[1]*t_obs/texp_N_n)*(nP_N_AT)/sqrt(2*nPb_N_AT+nP_N_AT+2*n_pix_P_N*(RON_N^2));+struc_function(nPb_N_AT, t_chop))
      SNRn_fund=SNRn_N_AT_t
   endif else begin ;High_Sens mode
      SNRn_N_AT = (nP_N_AT)/sqrt(2*nPb_N_AT+nP_N_AT+2*n_pix_P_N*(RON_N^2)+var_eps_N);+struc_function(nPb_N_AT, t_chop))
      SNRn_N_AT_t = sqrt(R_chop[1]*t_obs_phot/texp_N_n)*(nP_N_AT)/sqrt(2*nPb_N_AT+nP_N_AT+2*n_pix_P_N*(RON_N^2)+var_eps_N);+struc_function(nPb_N_AT, t_chop))
      SNRn_fund=SNRn_N_AT_t
   endelse

   ;Coherent flux SNR
   SNRc_N_AT=fltarr(nw_N)
   SNRc_N_AT_t=fltarr(nw_N)
   SNRv_N_AT=fltarr(nw_N)
   if keyword_set(TAB_LAM_MATISSE) then vis_N=replicate(vis[0],nw_N) else vis_N=vis

   if mode eq 0 then begin ;Si_Phot mode
      if coherent_int eq 0 then begin ;no coherent integration
         SNRc_N_AT = (nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_AT_t = sqrt(R_chop[1]*t_obs/texp_N_c)*(nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) ;+(nI_N_AT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_AT(i)+nI_N_AT(i)))
      endif else begin ;Coherent integration over nframes
         SNRc_N_AT = (nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_AT_t = sqrt(R_chop[1]*t_obs/texp_N_c)*(nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) ;+(nI_N_AT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_AT(i)+nI_N_AT(i)))
      endelse
   endif else begin
      if coherent_int eq 0 then begin ;High_Sens mode
         SNRc_N_AT = (nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_AT_t = sqrt(t_obs/texp_N_c)*(nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) ;+(nI_N_AT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_AT(i)+nI_N_AT(i)))
      endif else begin ;Coherent integration over nframes
         SNRc_N_AT = (nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_AT_t = sqrt(t_obs/texp_N_c)*(nI_N_AT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)) ;+(nI_N_AT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_AT(i)+nI_N_AT(i)))
      endelse
   endelse

   SNRv_N_AT = 1./(sqrt((1./SNRc_N_AT_t)^2+(1./SNRn_N_AT_t)^2))

   print,'                                                                  '
   print,'*************************************************************************'
   print,'************************* N BAND ****************************************'
   print,'*************************************************************************'  
   print,'SNR_C in N band with ATs (per frame) = ',SNRc_N_AT
 
   ;Relative error on the visibility
   SNRv_N_AT_pourcent = (1./SNRv_N_AT)*100
    
   ;Relative error and SNR on the calibrated visibility and squared visibility (fundamental and calibrated terms)
   error_v_fund=fltarr(nw_N,/nozero)
   error_v_cal=fltarr(nw_N,/nozero)
   error_v_fund = SNRv_N_AT_pourcent                                                              ;Relative error on V in % (fundamental noise)
   error_v_cal = sqrt(pine_N_AT_pourcent^2+strehl_N_AT_pourcent^2+OPD_jitter_N_AT_pourcent^2)     ;Relative error on V in % (calibration)
   error_v=sqrt(error_v_fund^2+error_v_cal^2) ;Total relative error on V in %
   SNRv = (1./error_v)*100
   SNRv_fund = (1./error_v_fund)*100


       ;Squared coherent flux variance
       VARc2_N_AT=fltarr(nw_N)
       if mode eq 0 then begin ;Si_Phot mode
          if coherent_int then begin ;Coherent integration over nframes
             VARc2_N_AT=nI_N_AT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_AT+nI_N_AT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_AT+nI_N_AT)^2+n_T*(nIb_N_AT+nI_N_AT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_AT=(nI_N_AT*vis_N*Vinst_N)^2/sqrt(VARc2_N_AT)
             SNRc2_N_AT_t=sqrt(R_chop[1]*t_obs/texp_N_c)*SNRc2_N_AT
             SNRc2_fund=SNRc2_N_AT_t
          endif else begin
             VARc2_N_AT=nI_N_AT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_AT+nI_N_AT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_AT+nI_N_AT)^2+n_T*(nIb_N_AT+nI_N_AT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_AT=(nI_N_AT*vis_N*Vinst_N)^2/sqrt(VARc2_N_AT)
             SNRc2_N_AT_t=sqrt(R_chop[1]*t_obs/texp_N_c)*SNRc2_N_AT
             SNRc2_fund=SNRc2_N_AT_t
          endelse
       endif else begin
          if coherent_int then begin ;Coherent integration over nframes
             VARc2_N_AT=nI_N_AT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_AT+nI_N_AT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_AT+nI_N_AT)^2+n_T*(nIb_N_AT+nI_N_AT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_AT=(nI_N_AT*vis_N*Vinst_N)^2/sqrt(VARc2_N_AT)
             SNRc2_N_AT_t=sqrt(t_obs/texp_N_c)*SNRc2_N_AT
             SNRc2_fund=SNRc2_N_AT_t
          endif else begin
             VARc2_N_AT=nI_N_AT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_AT+nI_N_AT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_AT+nI_N_AT)^2+n_T*(nIb_N_AT+nI_N_AT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_AT=(nI_N_AT*vis_N*Vinst_N)^2/sqrt(VARc2_N_AT)
             SNRc2_N_AT_t=sqrt(t_obs/texp_N_c)*SNRc2_N_AT
             SNRc2_fund=SNRc2_N_AT_t
          endelse
       endelse

       
       ;Photometry variance
       VARn_N_AT=fltarr(nw_N)
       VARn_N_AT=2*nPb_N_AT + nP_N_AT + 2*n_pix_P_N*(RON_N^2) + var_eps_N;struc_function(nPb_N_AT, t_chop)
       SNRn_N_AT=nP_N_AT/sqrt(VARn_N_AT)
       print,'Db = ',struc_function(nPb_N_AT, t_chop)
       if mode eq 0 then begin ;Si_Phot mode
          SNRn_N_AT_t=sqrt(R_chop[1]*t_obs/texp_N_n)*SNRn_N_AT
       endif else begin ;High_Sens mode
          SNRn_N_AT_t=sqrt(R_chop[1]*t_obs_phot/texp_N_n)*SNRn_N_AT
       endelse


       ;Relative error on squared visibility
       error_rel_v2_N_AT=sqrt((1./SNRc2_N_AT)^2+2.*(1./SNRn_N_AT^2))
       error_rel_v2_N_AT_t=sqrt((1./SNRc2_N_AT_t)^2+2.*(1./SNRn_N_AT_t^2))

       ;SNR on squared visibility
       SNRv2_N_AT=1./error_rel_v2_N_AT
       SNRv2_N_AT_t=1./error_rel_v2_N_AT_t

       ;Relative error and SNR on the calibrated squared visibility (fundamental and calibrated terms)
       error_v2_fund=fltarr(nw_N,/nozero)
       error_v2_cal=fltarr(nw_N,/nozero)
       error_v2_fund = error_rel_v2_N_AT_t*100. ;Relative error on V in % (fundamental noise)
       error_v2_cal = sqrt(pine_N_AT_pourcent^2+strehl_N_AT_pourcent^2+OPD_jitter_N_AT_pourcent^2) ;Relative error on V in % (calibration)
       error_v2=sqrt(error_v2_fund^2+error_v2_cal^2) ;Total relative error on V in %
       SNRv2 = (1./error_v2)*100
       SNRv2_fund = (1./error_v2_fund)*100


   ;Absolute error on the differential phase (in rad)
   error_phi_fund=fltarr(nw_N)
   error_phi_cal=fltarr(nw_N)
   error_phi=fltarr(nw_N)

   error_phi_cal=sqrt(pine_N_phi_AT^2+strehl_N_phi_AT^2+opd_jitter_N_phi_AT^2+chro_N_phi_AT^2) ;additive error term taking into account instrumental and atmospheric errors
                                ;error_phi_cal=sqrt(chro_N_phi_AT^2)
   ;;additive error term taking into
   ;;account instrumental and atmospheric errors
   if mode eq 0 then begin  ;Si_phot mode
      error_phi_fund = sqrt((texp_N_phi/(2*R_chop[1]*t_obs))*(1./SNRc_N_AT^2)) ;error due to fundamental noises
   endif else begin
      error_phi_fund = sqrt((texp_N_phi/(2*t_obs))*(1./SNRc_N_AT^2)) ;error due to fundamental noises
   endelse

   error_phi=sqrt(error_phi_fund^2+error_phi_cal^2)                                            ;Total absolute error (in rad)


   ;Error on the closure phase (in rad)
   error_c_fund=fltarr(nw_N)
   error_c_cal=fltarr(nw_N)
   error_c=fltarr(nw_N)

   ;to be checked
   ;error_c_cal=sqrt(pine_N_clo_AT^2+strehl_N_clo_AT^2+det_N_clo_AT^2)         ;additive error term taking into account instrumental and atmospheric errors (rad)
   error_c_cal=sqrt(det_N_clo_AT^2)         ;additive error term taking into account instrumental and atmospheric errors (rad)
   if mode eq 0 then begin  ;Si_phot mode
      error_c_fund = sqrt((3.*texp_N_phi/(2*R_chop[1]*t_obs))*(1./SNRc_N_AT^2)) ;Absolute error due to fundamental noises (rad)
   endif else begin
      error_c_fund = sqrt((3.*texp_N_phi/(2*t_obs))*(1./SNRc_N_AT^2)) ;Absolute error due to fundamental noises (rad)
   endelse
   error_c=sqrt(error_c_fund^2+error_c_cal^2)

   openw,1,'errv_vs_flux_N_AT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],error_v2_fund[i]/100.,(error_v2_fund[i]/100.)/(2.*vis_N[i]*Vinst_N)
   close,1

   openw,1,'errcorr_ratio_vs_flux_N_AT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],(sqrt(2)/2.)*(1./SNRc2_N_AT_t[i]),sqrt(2)*(1./SNRc_N_AT_t[i])
   close,1

   openw,1,'errcorr_vs_flux_N_AT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],tab_lambda_N[i],(source_flux_N[i]*Vinst_N*vis_N[i])^2/SNRc2_N_AT_t[i],((source_flux_N[i]*Vinst_N*vis_N[i])^2/SNRc2_N_AT_t[i])*1./(2.*vis_N[i]*Vinst_N*source_flux_N[i])
   close,1

   openw,1,'errcorr_rel_vs_flux_N_AT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],1./SNRc2_N_AT_t[i],1./(SNRc2_N_AT_t[i]*2.)
   close,1

   openw,1,'errphi_vs_flux_N_AT.dat'
   for i=0,n_elements(error_c)-1 do printf,1,source_flux_N[i],error_phi_fund[i]*180./!pi
   close,1

   openw,1,'errc_vs_flux_N_AT.dat'
   for i=0,n_elements(error_c)-1 do printf,1,source_flux_N[i],error_c[i]*180./!pi
   close,1

   print,'----------------------------------------------------------------------------'
   print,'----------------------------------------------------------------------------'
   print,'SNR and error estimations (with ATs) in N band'
   print,'----------------------------------------------------------------------------'
   print,'----------------------------------------------------------------------------'
   print,' wavelength = ',tab_lambda_N
   tab_lambda_final=tab_lambda_N
   print,' nb_photon_source = ',nphoton_N_AT
   print,' nb_photon_background = ',nbackground_N_AT
   ;print,'detector noise (photometry channel) = ',n_pix_P_N*(RON_N^2)
   print,'detector noise (interferometry channel) = ',n_pix_I_N*(RON_N^2)
   ;print,'Noise-to-signal-ratio (readout noise) = ',sqrt(n_pix_I_N*(RON_N^2))/nphoton_N_AT
   ;print,'Total noise (interferometry channel) = ',n_T*nIb_N_AT+n_T*nI_N_AT+n_pix_I_N*(RON_N^2)
   print,'----------------------------------------------------------------------------'
   print,'Object visibility  = ',vis_N
   ;print,'SNR on the non-calibrated visibility  = ',SNRv_fund
   ;print,'SNR on the calibrated visibility  = ',SNRv
   ;print,'SNR on the non-calibrated squared visibility  = ',SNRv2_fund
   ;print,'SNR on the calibrated squared visibility  = ',SNRv2
   print,'Coherent flux SNR  = ',SNRc_N_AT_t
   print,'Squared coherent flux SNR  = ',SNRc2_N_AT_t
   print,'Photometry SNR  = ',SNRn_N_AT
   print,'Relative error on the fundamental visibility [%] = ',error_v_fund
   print,'Relative error on the calibrated visibility [%] = ',error_v
   print,'Relative error on the fundamental squared visibility [%] = ',error_v2_fund
   print,'Relative error on the calibrated squared visibility [%] = ',error_v2
   print,'Total error on the differential phase (rad) = ',error_phi
   print,'fundamental error on the differential phase (rad) = ',error_phi_fund
   print,'SNR on the coherent flux = ',1./(sqrt(2)*error_phi_fund)
   print,'Total error on the closure phase (rad ) = ',error_c
   print,'fundamental error on the closure phase (rad) = ',error_c_fund
   print,'----------------------------------------------------------------------------'
   print,'                                                                            '

endif


if telescope eq 1 then begin ;we choose UT
    
   ;Instrumental visibility
   if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then Vinst_N=Vinst_N_UT[0] else Vinst_N=Vinst_N_UT[1]

   ;number of photons from the source
   if keyword_set(add_trans) then begin
      a=execute('resolution=R_'+Res_N)
      readcol,Ta_N,lam,trans_atm
      n_smooth=fix(Res_spec/resolution)
      trans_atm=smooth(trans_atm,n_smooth)
      trans_atm_N=interpol(trans_atm,lam,tab_lambda_N*1e+6,/spline)
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then Trans_N_UT=trans_source(SR_N,trans_atm_N,TI_N_UT,TW_N,TC_N[0],TP_N_UT,TS_N_UT) else Trans_N_UT=trans_source(SR_N,trans_atm_N,TI_N_UT,TW_N,TC_N[1],TP_N_UT,TS_N_UT)
      nphoton_N_UT = Trans_N_UT*S_tel_UT*delta_lambda_N*DIT*source_flux_N_W*tab_lambda_N/(h_planck*c)
   endif else begin
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then nphoton_N_UT = Trans_N_UT[0]*S_tel_UT*delta_lambda_N*DIT*source_flux_N_W*tab_lambda_N/(h_planck*c) else nphoton_N_UT = Trans_N_UT[1]*S_tel_UT*delta_lambda_N*DIT*source_flux_N_W*tab_lambda_N/(h_planck*c)
   endelse
   print,'Trans_N_UT = ',Trans_N_UT
   ;number of photon of the background
   if keyword_set(add_trans) then begin
      Eps_N_UT=emiss_bg(trans_atm_N,TI_N_UT,TW_N,Cp_N_UT)
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then nbackground_N_UT = Transb_N[0]*Eps_N_UT*S_tel_UT*delta_Omega_N_UT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c) else nbackground_N_UT = Transb_N[1]*Eps_N_UT*S_tel_UT*delta_Omega_N_UT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c)
         print,'Transb_N[0]',Transb_N[0]
         print,'Trans_atm_N = ',Trans_atm_N
         print,'Emissivity = ',Eps_N_UT
         print,'S_tel_UT = ',S_tel_UT
         print,'delta_Omega_N_UT = ',delta_Omega_N_UT
         print,'delta_lambda_N = ',delta_lambda_N
         print,'sky emission = ',blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c)
   endif else begin
      if strcmp(Res_N,'N_low',/FOLD_CASE) eq 1 then nbackground_N_UT = Transb_N[0]*Eps_N_UT*S_tel_UT*delta_Omega_N_UT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c) else nbackground_N_UT = Transb_N[1]*Eps_N_UT*S_tel_UT*delta_Omega_N_UT*delta_lambda_N*DIT*blackbody(tab_lambda_N,Temp)*tab_lambda_N/(h_planck*c)
   endelse

   print,'nb photons background = ',nbackground_N_UT/(QE_N*DIT)

    
   ;Split of the photon numbers in the interferometric and photometric channels    
   nP_N_UT = nphoton_N_UT*alpha_P         ;photon number in L band, for UT, in the photometric channel
   nPb_N_UT = nbackground_N_UT*alpha_P    ;background photon number in L band, for UT, in the photometric channel
   nI_N_UT = nphoton_N_UT*alpha_I         ;photon number in L band, for UT, in the interferometric channel
   nIb_N_UT = nbackground_N_UT*alpha_I    ;background photon number in L band, for UT, in the interferometric channel 

   ;Photometry SNR
   if mode eq 0 then begin ;Si_Phot mode
      SNRn_N_UT = (nP_N_UT)/sqrt(2*nPb_N_UT+nP_N_UT+2*n_pix_P_N*(RON_N^2));+struc_function(nPb_N_UT, t_chop))
      SNRn_N_UT_t = sqrt(R_chop[1]*t_obs/texp_N_n)*(nP_N_UT)/sqrt(2*nPb_N_UT+nP_N_UT+2*n_pix_P_N*(RON_N^2));+struc_function(nPb_N_UT, t_chop))
      SNRn_fund=SNRn_N_AT_t
   endif else begin ;High_Sens mode
      SNRn_N_UT = (nP_N_UT)/sqrt(2*nPb_N_UT+nP_N_UT+2*n_pix_P_N*(RON_N^2));+struc_function(nPb_N_UT, t_chop))
      SNRn_N_UT_t = sqrt(R_chop[1]*t_obs_phot/texp_N_n)*(nP_N_UT)/sqrt(2*nPb_N_UT+nP_N_UT+2*n_pix_P_N*(RON_N^2));+struc_function(nPb_N_UT, t_chop))
      SNRn_fund=SNRn_N_AT_t
   endelse

   ;Coherent flux SNR
   SNRc_N_UT=fltarr(nw_N)
   SNRc_N_UT_t=fltarr(nw_N)
   SNRv_N_UT=fltarr(nw_N)
   if keyword_set(TAB_LAM_MATISSE) then vis_N=replicate(vis[0],nW) else vis_N=vis
   

   if mode eq 0 then begin            ;Si_Phot mode
      if coherent_int eq 0 then begin ;no coherent integration
         SNRc_N_UT = (nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_UT_t = sqrt(R_chop[1]*t_obs/texp_N_c)*(nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) ;+(nI_N_UT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_UT(i)+nI_N_UT(i)))
      endif else begin ;Coherent integration over nframes
         SNRc_N_UT = (nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_UT_t = sqrt(R_chop[1]*t_obs/texp_N_c)*(nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) ;+(nI_N_UT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_UT(i)+nI_N_UT(i)))
      endelse
   endif else begin
      if coherent_int eq 0 then begin ;High_Sens mode
         SNRc_N_UT = (nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_UT_t = sqrt(t_obs/texp_N_c)*(nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) ;+(nI_N_UT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_UT(i)+nI_N_UT(i)))
      endif else begin ;Coherent integration over nframes
         SNRc_N_UT = (nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) 
         SNRc_N_UT_t = sqrt(t_obs/texp_N_c)*(nI_N_UT*vis_N*Vinst_N)/sqrt(n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)) ;+(nI_N_UT(i)*(*oivis(i).visamp))/(n_T*beta*gamma*(nIb_N_UT(i)+nI_N_UT(i)))
      endelse
   endelse

   SNRv_N_UT = 1./(sqrt((1./SNRc_N_UT_t)^2+(1./SNRn_N_UT_t)^2))

   print,'                                                                  '
   print,'*************************************************************************'
   print,'************************* N BAND ****************************************'
   print,'*************************************************************************'
   print,'Magnitude of the source: N=',m_N
   print,'Signal (C) = ',(nI_N_UT*vis_N*Vinst_N)
   print,'nI = ',nI_N_UT
   print,'vis_N = ',vis_N
   print,'Vinst_N = ',Vinst_N
   print,'Var_C in N band with UTs (per frame) = ',n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)
   print,'SNR_C in N band with UTs (per frame) = ',SNRc_N_UT
 
   ;Relative error on the visibility
   SNRv_N_UT_pourcent = (1./SNRv_N_UT)*100
    
   ;Relative error and SNR on the calibrated visibility and squared visibility (fundamental and calibrated terms)
   error_v_fund=fltarr(nw_N,/nozero)
   error_v_cal=fltarr(nw_N,/nozero)
   error_v_fund = SNRv_N_UT_pourcent                                                              ;Relative error on V in % (fundamental noise)
   error_v_cal = sqrt(pine_N_UT_pourcent^2+strehl_N_UT_pourcent^2+OPD_jitter_N_UT_pourcent^2)     ;Relative error on V in % (calibration)
   error_v=sqrt(error_v_fund^2+error_v_cal^2) ;Total relative error on V in %
   SNRv = (1./error_v)*100
   SNRv_fund = (1./error_v_fund)*100


       ;Squared coherent flux variance
       VARc2_N_UT=fltarr(nw_N)
       if mode eq 0 then begin ;Si_Phot mode
          if coherent_int then begin ;Coherent integration over nframes
             VARc2_N_UT=nI_N_UT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_UT+nI_N_UT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_UT+nI_N_UT)^2+n_T*(nIb_N_UT+nI_N_UT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_UT=(nI_N_UT*vis_N*Vinst_N)^2/sqrt(VARc2_N_UT)
             SNRc2_N_UT_t=sqrt(R_chop[1]*t_obs/texp_N_c)*SNRc2_N_UT
             SNRc2_fund=SNRc2_N_UT_t
          endif else begin
             VARc2_N_UT=nI_N_UT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_UT+nI_N_UT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_UT+nI_N_UT)^2+n_T*(nIb_N_UT+nI_N_UT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_UT=(nI_N_UT*vis_N*Vinst_N)^2/sqrt(VARc2_N_UT)
             SNRc2_N_UT_t=sqrt(R_chop[1]*t_obs/texp_N_c)*SNRc2_N_UT
             SNRc2_fund=SNRc2_N_UT_t
          endelse
       endif else begin
          if coherent_int then begin ;Coherent integration over nframes
             VARc2_N_UT=nI_N_UT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_UT+nI_N_UT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_UT+nI_N_UT)^2+n_T*(nIb_N_UT+nI_N_UT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_UT=(nI_N_UT*vis_N*Vinst_N)^2/sqrt(VARc2_N_UT)
             SNRc2_N_UT_t=sqrt(t_obs/texp_N_c)*SNRc2_N_UT
             SNRc2_fund=SNRc2_N_UT_t
          endif else begin
             VARc2_N_UT=nI_N_UT^2*(vis_N*Vinst_N)^2*(2.*n_T*(nIb_N_UT+nI_N_UT)+2.*n_pix_I_N*RON_N^2+4.)+n_T^2*(nIb_N_UT+nI_N_UT)^2+n_T*(nIb_N_UT+nI_N_UT)*(1.+2.*n_pix_I_N*RON_N^2)+(3.+n_pix_I_N)*n_pix_I_N*RON_N^4. 
             SNRc2_N_UT=(nI_N_UT*vis_N*Vinst_N)^2/sqrt(VARc2_N_UT)
             SNRc2_N_UT_t=sqrt(t_obs/texp_N_c)*SNRc2_N_UT
             SNRc2_fund=SNRc2_N_UT_t
          endelse
       endelse
       
       ;Photometry variance
       VARn_N_UT=fltarr(nw_N)
       VARn_N_UT=2*nPb_N_UT+nP_N_UT+2*n_pix_P_N*(RON_N^2);+struc_function(nPb_N_UT, t_chop)
       print,'Db = ',struc_function(nPb_N_UT, t_chop)
       SNRn_N_UT=nP_N_UT/sqrt(VARn_N_UT)
       if mode eq 0 then begin ;Si_Phot mode
          SNRn_N_UT_t=sqrt(R_chop[1]*t_obs/texp_N_n)*SNRn_N_UT
       endif else begin ;High_Sens mode
          SNRn_N_UT_t=sqrt(R_chop[1]*t_obs_phot/texp_N_n)*SNRn_N_UT
       endelse
       print,'Signal (photometry) = ',nP_N_UT
       print,'Var_n in N band with UTs (per frame) = ',VARn_N_UT
       print,'SNR_n in N band with UTs (per frame) = ',SNRn_N_UT

       ;Relative error on squared visibility
       error_rel_v2_N_UT=sqrt((1./SNRc2_N_UT)^2+2.*(1./SNRn_N_UT^2))
       error_rel_v2_N_UT_t=sqrt((1./SNRc2_N_UT_t)^2+2.*(1./SNRn_N_UT_t^2))

       ;SNR on squared visibility
       SNRv2_N_UT=1./error_rel_v2_N_UT
       SNRv2_N_UT_t=1./error_rel_v2_N_UT_t

       ;Relative error and SNR on the calibrated squared visibility (fundamental and calibrated terms)
       error_v2_fund=fltarr(nw_N,/nozero)
       error_v2_cal=fltarr(nw_N,/nozero)
       error_v2_fund = error_rel_v2_N_UT_t*100. ;Relative error on V in % (fundamental noise)
       error_v2_cal = sqrt(pine_N_UT_pourcent^2+strehl_N_UT_pourcent^2+OPD_jitter_N_UT_pourcent^2) ;Relative error on V in % (calibration)
       error_v2=sqrt(error_v2_fund^2+error_v2_cal^2) ;Total relative error on V in %
       SNRv2 = (1./error_v2)*100
       SNRv2_fund = (1./error_v2_fund)*100


   ;Absolute error on the differential phase (in rad)
   error_phi_fund=fltarr(nw_N)
   error_phi_cal=fltarr(nw_N)
   error_phi=fltarr(nw_N)

   error_phi_cal=sqrt(pine_N_phi_UT^2+strehl_N_phi_UT^2+opd_jitter_N_phi_UT^2+chro_N_phi_UT^2) ;additive error term taking into account instrumental and atmospheric errors
   ;error_phi_cal=sqrt(chro_N_phi_UT^2)
                                ;;additive error term taking into account instrumental and atmospheric errors
   if mode eq 0 then begin  ;Si_phot mode
      error_phi_fund = sqrt((texp_N_phi/(2*R_chop[1]*t_obs))*(1./SNRc_N_UT^2)) ;error due to fundamental noises
   endif else begin
       error_phi_fund = sqrt((texp_N_phi/(2*t_obs))*(1./SNRc_N_UT^2)) ;error due to fundamental noises
   endelse

   error_phi=sqrt(error_phi_fund^2+error_phi_cal^2)                                            ;Total absolute error (in rad)


   ;Error on the closure phase (in rad)
   error_c_fund=fltarr(nw_N)
   error_c_cal=fltarr(nw_N)
   error_c=fltarr(nw_N)

   ;to be checked
   ;error_c_cal=sqrt(pine_N_clo_UT^2+strehl_N_clo_UT^2+det_N_clo_UT^2)         ;additive error term taking into account instrumental and atmospheric errors (rad)
   error_c_cal=sqrt(det_N_clo_UT^2)         ;additive error term taking into account instrumental and atmospheric errors (rad)

   if mode eq 0 then begin ;Si_phot mode
      error_c_fund = sqrt((3.*texp_N_phi/(2*R_chop[1]*t_obs))*(1./SNRc_N_UT^2)) ;Absolute error due to fundamental noises (rad)
   endif else begin
      error_c_fund = sqrt((3.*texp_N_phi/(2*t_obs))*(1./SNRc_N_UT^2)) ;Absolute error due to fundamental noises (rad)
   endelse

   error_c=sqrt(error_c_fund^2+error_c_cal^2)
 
   openw,1,'errv_vs_flux_N_UT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],error_v2_fund[i]/100.,(error_v2_fund[i]/100.)/(2.*vis_N[i]*Vinst_N)
   close,1

   openw,1,'errcorr_ratio_vs_flux_N_UT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],sqrt(2)*(1./SNRc2_N_UT_t[i])/2.,sqrt(2)*(1./SNRc_N_UT_t[i])
   close,1

   openw,1,'errcorr_vs_flux_N_UT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],tab_lambda_N[i],(source_flux_N[i]*Vinst_N*vis_N[i])^2/SNRc2_N_UT_t[i],((source_flux_N[i]*Vinst_N*vis_N[i])^2/SNRc2_N_UT_t[i])*1./(2.*vis_N[i]*Vinst_N*source_flux_N[i])
   close,1

   openw,1,'errcorr_rel_vs_flux_N_UT.dat'
   for i=0,n_elements(error_v)-1 do printf,1,source_flux_N[i],1./SNRc2_N_UT_t[i],1./(SNRc2_N_UT_t[i]*2.)
   close,1

   openw,1,'errphi_vs_flux_N_UT.dat'
   for i=0,n_elements(error_c)-1 do printf,1,source_flux_N[i],error_phi_fund[i]*180./!pi
   close,1

   openw,1,'errc_vs_flux_N_UT.dat'
   for i=0,n_elements(error_c)-1 do printf,1,source_flux_N[i],error_c[i]*180./!pi
   close,1


   print,'----------------------------------------------------------------------------'
   print,'----------------------------------------------------------------------------'
   print,'SNR and error estimations (with UTs) in N band'
   print,'----------------------------------------------------------------------------'
   print,'----------------------------------------------------------------------------'
   print,' wavelength = ',tab_lambda_N
   tab_lambda_final=tab_lambda_N
   print,' nb_photon_source = ',nphoton_N_UT
   print,' nb_photon_background = ',nbackground_N_UT
   ;print,'detector noise (photometry channel) = ',n_pix_P_N*(RON_N^2)
   ;print,'detector noise (interferometry channel) = ',n_pix_I_N*(RON_N^2)
   ;print,'Noise-to-signal-ratio (readout noise) = ',sqrt(n_pix_I_N*(RON_N^2))/nphoton_N_UT
   ;print,'Total noise (interferometry channel) = ',n_T*nIb_N_UT+n_T*nI_N_UT+n_pix_I_N*(RON_N^2)
   print,'----------------------------------------------------------------------------'
   print,'Object visibility  = ',vis_N
   print,'SNR on the non-calibrated visibility  = ',SNRv_fund
   print,'SNR on the calibrated visibility  = ',SNRv
   print,'SNR on the non-calibrated squared visibility  = ',SNRv2_fund
   print,'SNR on the calibrated squared visibility  = ',SNRv2
   print,'Relative error on the fundamental visibility [%] = ',error_v_fund
   print,'Relative error on the calibrated visibility [%] = ',error_v
   print,'Relative error on the fundamental visibility [%] = ',error_v2_fund
   print,'Relative error on the calibrated visibility [%] = ',error_v2
   print,'Total error on the differential phase (rad) = ',error_phi
   print,'fundamental error on the differential phase (rad) = ',error_phi_fund
   print,'SNR on the coherent flux = ',1./(sqrt(2)*error_phi_fund)
   print,'Total error on the closure phase (rad )= ',error_c
   print,'fundamental error on the closure phase (rad)= ',error_c_fund
   print,'----------------------------------------------------------------------------'
   print,'                                                                            '

endif


print,'tab_lambda_final = ',tab_lambda_final

   set_plot,'ps'
   !p.multi=[0,2,2]
   Device,Decomposed=0,color=1,BITS_PER_PIXEL=8
   loadct,39
   device,filename='SNR_N.eps',/ENCAPSULATED
   plot,tab_lambda_final*1e+6,SNRc2_fund,thick=2,xtitle='wavelength [um]',ytitle='SNR C2',xstyle=1,xr=[8.0,13.0]
   plot,tab_lambda_final*1e+6,SNRn_fund,thick=2,xtitle='wavelength [um]',ytitle='SNR photometry',xstyle=1,xr=[8.0,13.0]
   plot,tab_lambda_final*1e+6,SNRv2_fund,thick=2,title='Fundamental SNR on V2',xtitle='wavelength [um]',ytitle='SNR V2',xstyle=1,xr=[8.0,13.0]
   plot,tab_lambda_final*1e+6,error_c_fund*180./!pi,thick=2,title='Closure phase error',xtitle='wavelength [um]',ytitle='Closure phase error [deg]',xstyle=1,xr=[8.0,13.0]
   device,/close

;set_plot,'ps'
;device,filename='SNRv2_fund_N.ps'
;plot,tab_lambda_final*1e+6,SNRv2_fund,thick=2,title='Fundamental SNR on V2',xtitle='wavelength [um]',ytitle='SNR V2',psym=4,xstyle=1,xr=[8,13]
;device,/close

;set_plot,'ps'
;device,filename='SNRc2_fund_N.ps'
;plot,tab_lambda_final*1e+6,SNRc2_fund,thick=2,title='Fundamental SNR on c2',xtitle='wavelength [um]',ytitle='SNR C2',psym=4,xstyle=1,xr=[8,13]
;device,/close

;set_plot,'ps'
;device,filename='SNRn_fund_N.ps'
;plot,tab_lambda_final*1e+6,SNRn_fund,thick=2,title='Fundamental SNR on photometry',xtitle='wavelength [um]',ytitle='SNR photometry',psym=4,xstyle=1,xr=[8,13]
;device,/close

;set_plot,'ps'
;device,filename='errort3_fund_N.ps'
;plot,tab_lambda_final*1e+6,error_c_fund*180./!pi,thick=2,title='Closure phase error',xtitle='wavelength [um]',ytitle='Closure phase error [deg]',psym=4,xstyle=1,xr=[8,13]
;device,/close


endif




end

