;
;>>>>>>>>>>>>> Declaration of the input variables (Do not modify this line!)<<<<<<<<
;

;pro param_matisse

;common param, mode, HighSens_photometry, telescope, n_T,Res_LM,Res_N,t_obs,t_obs_phot,coherent_int, DIT, t_exp_LM, t_exp_N

;-----------------------------------------------------------------------------------------------------------
;This file contains the parameters values to be modified to select an observing configuration and exposure times of a observation with MATISSE. Other MATISSE instrumental and environmental parameters can be found in the param_matisse_expert.pro. The latter should not be modified (unless you know what you are doing).  
;-----------------------------------------------------------------------------------------------------------
  

;-------------------------
;Observing configuration
;-------------------------

  ;Keyword for simulating SiPhot mode (set 0) or HighSens mode (set 1)
  mode = 1

  ;Keyword for selecting photometry measurement or not in HighSens mode (important for the error estimation on the differential phases and closure phases)
  HighSens_photometry = 0
		
  ;Keyword for simulating ATs (set 0) or UTs (set 1)
  telescope = 0

  ;Number of telescopes
  n_T = 4.

  ;Spectral resolution:
  ; low resolution: R_LM = 30, R_N = 30
  ; medium resolution: R_LM=500, R_N=220
  ; high resolution: R_LM=950
  ; very high resolution: R_LM=3400

  ;R_LM = 30.; L/M band
  ;R_N


  ;Spectral resolution:
  ; LM band : 'LM_low' (R=34), 'LM_med' (R=506)
  R_LM_low=34.
  R_LM_med=506.
  ; only L band : 'L_high' (R = 958.6)
  R_L_high=958.
  ; only L band : 'L_veryhigh' (R=3400)
  R_L_veryhigh=3400.
  ; only M band : 'M_veryhigh' (R=3400) 
  R_M_veryhigh=3400.

  ;N band : 'N_low' (R=29.7)
  R_N_low=30.
  ;N band : 'N_high' (R=218.2)
  R_N_high=220.

  Res_LM='LM_low'
  Res_N='N_high'



;----------------------
; Exposure times
;-----------------------

  t_obs = 4.*60. ;Total observing time in s (including the time on sky if chopping is performed)
  t_obs_phot = 2.*60.  ;Observing time on the photometry per beam (in s) when doing the subsequent 4 photometry measurements in High_Sens mode. This Observing time includes the time on sky if chopping is performed.
  coherent_int = 0  ;0: no coherent integration over nframes, 1: coherent integration over nframes
  nframes = 10. ;Number of frames over which the coherent integration is performed.

;-------------------------
; Detector time parameters
;-------------------------

  ;Possible values (in s) for the detector time parameters (det_time_parameters=[DIT,frame_time]):

  ;LM band

        ;Low resolution (Nochop):
        ;det_time_params=[0.02,0.037]
        ;det_time_params=[0.075,0.156]
        ;det_time_params=[0.111,0.222] 
        ;det_time_params=[1.,1.117] 

        ;Medium resolution (Nochop):
        ;det_time_params=[0.111,0.228]
        ;det_time_params=[1.3,2.374]
        ;det_time_params=[10.,11.074]

        ;High resolution (Nochop):
        ;det_time_params=[0.111,0.228]
        ;det_time_params=[1.3,2.585]
        ;det_time_params=[10.,11.285]

        ;Very High resolution (Nochop):
        ;det_time_params=[0.111,0.228]
        ;det_time_params=[10.,11.33]

  ;N band

        ;Low resolution :
        ;det_time_params=[0.02,0.026] 

        ;High resolution :
        det_time_params=[0.075,0.084]







  ;DIT = 1.;75e-3 ;Detector Integration Time (in s). This DIT includes the minimum time to read a given area of the detector + an additional integration time if needed and if possible (for instance if a fringe tracker is used). Usually, by default and without a fringe tracke, a DIT~30ms in L band should be safe in terms of coherence time. In N band, the coherence time is much longer (~300ms) but detector saturation becomes the limiting factor, thus preventing to go longer than DIT~30ms for one single frame. 


  ;minDIT = 20e-3 ;Minimum time (in s) required to read the detector. It depends on the spectral resolution and the detector. For instance, a minDIT of 20 ms corresponds to the minimum time to read a low resolution spectrum in L band (HAWAII detector).
  ;Total frame time in CDS readout mode, which includes the double read of the detector
  ;t_exp_LM = deadtime_LM + minDIT + DIT 
  ;t_exp_N = deadtime_N + minDIT + DIT 
  
;return
;end

;
;>>>>>>>>>>>>> End of input variable declaration (Do not modify this line!)<<<<<
;
