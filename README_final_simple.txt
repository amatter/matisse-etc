This package contains the various routines and files required to estimate MATISSE noise.
The main routine called 'SNR_analysis_chromatic_v9.pro' performs the error computation and SNR analysis for given total flux and associated visibility values, at given observing wavelengths. It returns various outputs (snr, error on the visibility, squared visibility, phase, closure phase). The SNR and error formula implemented in 'SNR_analysis_chromatic_v9.pro' is based on the MATISSE ETC specifications document and other JMMC documents.


FILES:
	- trans_atm_R60000_LM.txt : High-res atmospheric transmission spectrum in L/M band for a source observed at the zenith and median humidity conditions in Paranal 
	- trans_atm_R60000_N.txt  : same in N band.
	- jy2w.pro : routine to convert flux units (to put in one of your directories contained in the IDL_PATH)
	- param_matisse_V2.pro : File containing the parameters to be modified to choose an observing mode and configuration for MATISSE, prior to run the simulation routine
	- param_matisse_expert_V2.pro : File containing various MATISSE instrumental and environmental parameters that should not be modified at the moment (unless you really know what you are doing).
	- snr_analysis_chromatic_v9.pro : main routine that computes the expected errors and SNR of MATISSE  	

	 
GENERAL PROCEDURE TO FOLLOW: 
	- Select the desired observing configuration by modifying the associated parameters values in the param_matisse_V2.pro file. 
	- Compile the routine in an IDL interpreter: .r snr_analysis_chromatic_v9.pro
	- run the routine: snr_analysis_chromatic_v9, tab_lambda, vis, source_flux,error_v,error_v2,error_c,error_phi,ADD_TRANS=0/1,FIXED_WINDOW=0/1,NOISE_PER_PIX=0/1,MAG_SOURCE=0/1,TAB_LAM_MATISSE=0/1 (details on the input keyword parameters is given in the file at the beginning of the routine).
	  tab_lambda, vis, and source_flux are the input wavelength, visibility, and source flux values that have to be specified by the user when executing the routine snr_analysis_chromatic_v9.pro 	
	- The routine will produce various ascii files, and various outputs on screen
	

