;
;>>>>>>>>>>>>> Declaration of the input variables (Do not modify this line!)<<<<<<<<
;


;pro param_matisse_expert

;common param_expert, Temp, Trans_L_AT, Trans_L_UT, Trans_M_AT, Trans_M_UT, Trans_N_AT, Trans_N_UT, Transb_L, Transb_M, Transb_N, $
; SR_L,SR_M,SR_N,Ta_LM,Ta_N,TI_L_AT,TI_L_UT,TI_M_AT,TI_M_UT,TI_N_AT,TI_N_UT,TW_L,TW_M,TW_N,TC_L,TC_M,TC_N,TP_L_AT,TP_L_UT,TP_M_AT,TP_M_UT,TP_N_AT,TP_N_UT, $
;TS_L_AT,TS_L_UT,TS_M_AT,TS_M_UT,TS_N_AT,TS_N_UT, $
;  D_tel_AT, D_tel_UT, S_tel_AT, S_tel_UT, pinhole_size_LM, pinhole_size_N, delta_t, phi0, phi0_L, phi0_M, phi0_N, phi0_Jy, lambda_phi0, $
;Cp_L_AT, Cp_L_UT, Cp_M_AT, Cp_M_UT, Cp_N_AT, Cp_N_UT, Eps_L_AT, Eps_L_UT, Eps_M_AT, Eps_M_UT, Eps_N_AT, Eps_N_UT, $
;  R_chop, t_chop, deadtime_LM, deadtime_N, readout_time_N, overheads_factor_LM, overheads_factor_N, nPpix_LM, nPpix_N, nIpix_LM, nIpix_N, RON_LM, RON_N, beta, gamma, Vinst_L_AT, Vinst_L_UT, Vinst_M_AT, Vinst_M_UT, Vinst_N_AT, Vinst_N_UT, var_eps_N, pine_L_AT_pourcent, pine_L_UT_pourcent, pine_M_AT_pourcent, pine_M_UT_pourcent, $
;  pine_N_AT_pourcent, pine_N_UT_pourcent, strehl_L_AT_pourcent, strehl_L_UT_pourcent, strehl_M_AT_pourcent,$
;  strehl_M_UT_pourcent, strehl_N_AT_pourcent, strehl_N_UT_pourcent, OPD_jitter_L_AT_pourcent, OPD_jitter_L_UT_pourcent, $
;  OPD_jitter_M_AT_pourcent, OPD_jitter_M_UT_pourcent, OPD_jitter_N_AT_pourcent, OPD_jitter_N_UT_pourcent, $
;  pine_L_clo_AT,pine_M_clo_AT,pine_N_clo_AT,pine_L_clo_UT,pine_M_clo_UT,pine_N_clo_UT,strehl_L_clo_AT,strehl_M_clo_AT,strehl_N_clo_AT,strehl_L_clo_UT,strehl_M_clo_UT,strehl_N_clo_UT, $
;  det_L_clo_AT,det_M_clo_AT,det_N_clo_AT,det_L_clo_UT,det_M_clo_UT,det_N_clo_UT,pine_L_phi_AT,pine_M_phi_AT,pine_N_phi_AT,pine_L_phi_UT,pine_M_phi_UT,pine_N_phi_UT, $
;  strehl_L_phi_AT,strehl_M_phi_AT,strehl_N_phi_AT,strehl_L_phi_UT,strehl_M_phi_UT,strehl_N_phi_UT,opd_jitter_L_phi_AT,opd_jitter_M_phi_AT,opd_jitter_N_phi_AT,opd_jitter_L_phi_UT,opd_jitter_M_phi_UT,opd_jitter_N_phi_UT, $
; chro_L_phi_AT,chro_M_phi_AT,chro_N_phi_AT,chro_L_phi_UT,chro_M_phi_UT,chro_N_phi_UT,disp_LM_low,disp_LM_med,disp_L_high,disp_L_veryhigh,disp_M_veryhigh,disp_N_low,disp_N_high,QE_LM,QE_N



;----------------------------------------------------------------------------------------------
;This file contains the values of various MATISSE instrumental and
;environmental parameters. These values can be found 
;in the MATISSE performance analysis report document and should not be
;modified by the user. These values are likely to be modified/updated in the future.  
;----------------------------------------------------------------------------------------------
  

;----------------------
;Environment parameters
;----------------------
  ;Temperature of the focal lab environment in Kelvin
  Temp = 16.33+273.15 ;mean temperature of Paranal

  ;Coefficient due to pupil motion 
  Cp_L_AT=1.027
  Cp_L_UT=1.009
  Cp_M_AT=1.027
  Cp_M_UT=1.009
  Cp_N_AT=1.02
  Cp_N_UT=1.006

  ;global emissivity of the optical train for each spectral band and for each telescope
  Eps_L_AT = 0.78
  Eps_L_UT = 0.8
  Eps_M_AT = 0.85
  Eps_M_UT = 0.85
  Eps_N_AT = 0.79
  Eps_N_UT = 0.8


;-----------------------
;Transmission parameters
;-----------------------

;Atmospheric transmission Spectrum

Ta_LM='trans_atm_R60000_LM.txt'
Ta_N='trans_atm_R60000_N.txt'
Res_spec=60000. ;Spectral resolution of the atmospheric transmission spectrum



;Strehl ratio
SR_L=0.7
SR_M=0.8
SR_N=0.9

;Transmission of the interferometer down to the feeding optics inclusively
TI_L_AT=0.36
TI_L_UT=0.31
TI_M_AT=0.36
TI_M_UT=0.31
TI_N_AT=0.4
TI_N_UT=0.36
 
;Transmission of the MATISSE warm optics
TW_L=0.79
TW_M=0.79
TW_N=0.75

;Transmission of the MATISSE cold optics
;L band : [LR,MR,HR,VHR]
;M band : [LR,MR,VHR]
;N band : [LR,HR]
;TC_L=[0.28,0.28*0.6,0.28*0.6*0.52]
;TC_M=[0.28,0.28*0.6]
;TC_N=[0.3,0.3*0.9]
TC_L=[0.28,0.28*0.6,0.28*0.6*0.5,0.28*0.6*0.21]
TC_M=[0.28,0.28*0.6,0.28*0.6*1.18]
TC_N=[0.3,0.3*0.9]


;Transmission of the polarizer
TP_L_AT=1.;0.45
TP_L_UT=1.;0.45
TP_M_AT=1.;0.45
TP_M_UT=1.;0.45
TP_N_AT=1.;0.45
TP_N_UT=1.;0.45

;Transmission of the spatial filter assembly
TS_L_AT=0.40
TS_L_UT=0.38
TS_M_AT=0.42
TS_M_UT=0.42
TS_N_AT=0.62
TS_N_UT=0.65


;Overall transmission for a stellar source for each spectral band and
;for each telescope (for average conditions and atmospheric
;transmission)
;L band : [LR,MR,HR,VHR]
;M band : [LR,MR,VHR]
;N band : [LR,HR]
;Trans_L_AT = [0.0085,0.0085*0.6,0.0085*0.6*0.52] ;with polarizer
;Trans_L_UT = [0.007,0.007*0.6,0.007*0.6*0.52] ;with polarizer 
;Trans_M_AT = [0.0075,0.0075*0.6] ;with polarizer
;Trans_M_UT = [0.0065,0.0075*0.6] ;with polarizer
;Trans_N_AT = [0.018,0.018*0.9] ;with polarizer
;Trans_N_UT = [0.017,0.017*0.9] ;with polarizer
Trans_L_AT = [0.019,0.019*0.6,0.019*0.6*0.5,0.019*0.6*0.21] ;no polarizer
Trans_L_UT = [0.015,0.015*0.6,0.015*0.6*0.5,0.015*0.6*0.21] ;no polarizer 
Trans_M_AT = [0.016,0.016*0.6,0.016*0.6*1.18] ;no polarizer
Trans_M_UT = [0.014,0.014*0.6,0.014*0.6*1.18] ;no polarizer
Trans_N_AT = [0.04,0.04*0.9] ;no polarizer
Trans_N_UT = [0.0382,0.0382*0.9] ;no polarizer



;Overall transmission for the thermal background for each spectral
;band
;L band : [LR,MR,HR,VHR]
;M band : [LR,MR,VHR]
;N band : [LR,HR]
Transb_L = [0.2646,0.2646*0.6,0.2646*0.6*0.52,0.2646*0.6*0.21] ;without polarizer
Transb_M = [0.2646,0.2646*0.6,0.2646*0.6*1.18] ;without polarizer
Transb_N = [0.2835,0.2835*0.9] ;without polarizer
;Transb_L = [0.12,0.12*0.6,0.12*0.6*0.52] ;with polarizer
;Transb_M = [0.12,0.12*0.6] ;with polarizer
;Transb_N = [0.13,0.13*0.9] ;with polarizer


;N band Dispersion laws (pix/lambda)
tab_pix=findgen(2002)
disp_N_low=-33.2483 + tab_pix[385:641]*0.123553-tab_pix[385:641]^2*7.62003e-5
disp_N_high=7.443 + tab_pix[53:973]*5.831e-3+tab_pix[53:973]^2*2.772e-7

;L band Dispersion laws (pix/lambda)
disp_LM_low=5.560 + tab_pix[77:251] * 2.206e-3 - tab_pix[77:251]^2 * 5.818e-5
;disp_LM_low=11.0963487 - tab_pix[77:251] * 1.201035e-1 + tab_pix[77:251]^2 * 9.58891678e-4 - tab_pix[77:251]^3 * 3.7911241e-6 + tab_pix[77:251]^4 * 5.26357737e-9 
disp_LM_med=5.3541986 - tab_pix[41:2001]*1.42790804e-3 + tab_pix[41:2001]^2*6.75662379e-10
disp_L_high=4.280511214 - tab_pix[41:2001]*7.891339e-4 + tab_pix[41:2001]^2*2.93488058e-8
disp_L_veryhigh=4.14065123 - tab_pix[41:2001]*2.51977632e-4 + tab_pix[41:2001]^2*1.1381155e-8 
disp_M_veryhigh=5.15024977419274 - tab_pix[41:2001]*2.976680235e-4 + tab_pix[41:2001]^2*4.0096902e-9 - tab_pix[41:2001]^3*1.72964e-14 + tab_pix[41:2001]^4*9.31730249e-20
;disp_L_veryhigh=4.1943 - tab_pix[41:2001]*1.4983e-4
;disp_M_veryhigh=4.8044 - tab_pix[41:2001]*1.5163e-4

;----------------------
;Observation parameters
;----------------------  
  ;Surface telescope in m²
  S_tel_AT = 2.54
  S_tel_UT = 52.8;50.27
  ;Diameter telescope in m
  D_tel_AT = 1.8
  D_tel_UT = 8.2
  ;Size of the pinhole (spatial filter) in unit of lambda/D
  pinhole_size_LM = 1.5
  pinhole_size_N = 2.0

  
;reference flux at zero magnitude in W/m²/m
  phi0_L = 7.e-5 ;3.5 um
  phi0_M = 2.7e-5 ;4.5 um
  phi0_N = 1.e-6 ;10.5 um
  phi0 = [7.e-5,2.7e-5,1.e-6]
  lambda_phi0 = [3.5e-6,4.5e-6,10.5e-6]

;reference flux at zero magnitude in Jy
  ;phi0_L_Jy = 286.;288
  ;phi0_M_Jy = 182.;170
  ;phi0_N_Jy = 37.;36
  phi0_Jy = [286.,182.,37.]


;-------------------
;Chopping parameters
;-------------------
;chopping ratio in Hybrid mode (we consider only the non-chopped frames in LM band)
  R_chop = [1,0.5] ;[LM band,N band]]
  ;chopping time in seconds
  t_chop = 1./0.5


;-------------------
;Detector parameters
;-------------------
  QE_LM = 0.9 ;Detector quantum efficiency
  QE_N = 0.5  ;Detector quantum efficiency

  ;deadtime_LM = 10e-3 ;dead time added at the beginning of each frame (remanence, synchronization with piezos) for the HAWAII detector
  ;deadtime_N = 5e-3   ;dead time added at the beginning of each frame (remanence, synchronization with piezos) for the AQUARIUS detector
  ;readout_time_N = 8e-3 ;Minimum time to read the low-resolution spectrum in N band

  Overheads_factor_LM = 2.0 ;Factor to compute an approximate frame time for a given DIT (frame_time = overheads_factor*DIT) in CDS and SCI_SLOW_SPEED readout mode for the HAWAII detector -> To be refined
  Overheads_factor_N = 1.3  ;Factor to compute an approximate frame time for a given DIT (frame_time = overheads_factor*DIT) in snapshot readout mode for the AQUARIUS detector -> To be refined

 ;number of pixels used to analyze the signal in the photometric channel
 ;(average number computed for the central wavelength of the band)
  nPpix_N = 2977./6.
  nPpix_LM = 1550./6.

;number of pixels used to analyze the signal in the interferometric channel
;(average number computed for the central wavelength of the band)
  nIpix_N = 2977.
  nIpix_LM = 1550.

  ;Read-Out-Noise
  RON_LM = 10.1 ;slow readout mode
  ;RON_LM = 53.5 ;fast readout mode
  ;RON_N = 1800. ;low gain mode
  RON_N = 210. ;high gain mode




;-------------------
;Rejection parameter
;-------------------

  ;contamination factor of the fringe peaks by the thermal background due to detector windowing effects. It is considered at the moment as a constant value, which corresponds to the fraction of the thermal background-dominated low-frequency peak that contaminates the first fringe peak (namely the largest contamination), but should become eventually a function of spatial frequency.
  beta=1e-3	 	
  ;rejection factor of the thermal background. It is considered at the moment as a constant value, which corresponds to the residual background level reached at the position of the first fringe peak, but should become eventually a function of spatial frequency. 
  gamma = 5e-5;1e-7


;-----------------------------------------------------------------------------
;MATISSE Instrumental visibility [low spectral resolution,higher spectral resolutions]
;-----------------------------------------------------------------------------

;Without Fringe tracker (Perfo analysis)
;Vinst_L_AT = [0.61,0.69]
;Vinst_L_UT = [0.5,0.55]
;Vinst_M_AT = [0.72,0.79]
;Vinst_M_UT = [0.64,0.70]
;Vinst_N_AT = [0.45,0.48]
;Vinst_N_UT = [0.39,0.42]

;without Fringe tracker (Lab measurements)
Vinst_L_AT = [0.89,0.89]
Vinst_L_UT = [0.89,0.89]
Vinst_M_AT = [0.89,0.89]
Vinst_M_UT = [0.89,0.89]
Vinst_N_AT = [0.7,0.7]
Vinst_N_UT = [0.7,0.7]

;With a 'FINITO-like' Fringe tracker (Perfo analysis)
;Vinst_L_AT = [0.64,0.72]
;Vinst_L_UT = [0.48,0.53]
;Vinst_M_AT = [0.74,0.81]
;Vinst_M_UT = [0.62,0.68]
;Vinst_N_AT = [0.62,0.67]
;Vinst_N_UT = [0.60,0.65]

;--------------------------------------------
;Broadband error contribution on photometry 
;--------------------------------------------
;Variance of the broadband error affecting photometric measurements,
;especially in N-band. [in Photons/DIT]
var_eps_N = 0.0;1.5853634316e+7 


;--------------------------------------------
;Error contributions on calibrated visibility
;-------------------------------------------- 

  ;Error on calibrated visibility due to a variability of the pinholes images overlap (~ not the same overlap of the pinhole images between the observation of the source and its calibrator) for each spectral band and for each telescope
  pine_L_AT_pourcent = 0.5
  pine_L_UT_pourcent = 0.5
  pine_M_AT_pourcent = 0.3
  pine_M_UT_pourcent = 0.3
  pine_N_AT_pourcent = 0.5
  pine_N_UT_pourcent = 0.5
  ;Error on calibrated visibility due to a variation of the Strehl ratio between the observation of the source and the calibrator, for each spectral band and for each telescope
  strehl_L_AT_pourcent = 1.5
  strehl_L_UT_pourcent = 1.5  
  strehl_M_AT_pourcent = 1.
  strehl_M_UT_pourcent = 1.
  strehl_N_AT_pourcent = 0.9
  strehl_N_UT_pourcent = 0.9
  ;Error on calibrated visibility due to an OPD jitter between the two observations (source, calibrator) for each spectral band and each telescope
  OPD_jitter_L_AT_pourcent = 0.6
  OPD_jitter_L_UT_pourcent = 1.7
  OPD_jitter_M_AT_pourcent = 0.4
  OPD_jitter_M_UT_pourcent = 1.1
  OPD_jitter_N_AT_pourcent = 2.
  OPD_jitter_N_UT_pourcent = 2.6


;------------------------------------------------------------------------
;Error contributions (instrument+calibration) on calibrated closure phase
;------------------------------------------------------------------------

  ;Error on calibrated closure phase due to Pinhole overlap (in rad)
  pine_L_clo_AT = 6.1e-3
  pine_L_clo_UT = 6.1e-3
  pine_M_clo_AT = 3.7e-3
  pine_M_clo_UT = 3.7e-3
  pine_N_clo_AT = 6.1e-3
  pine_N_clo_UT = 6.1e-3

  ;Error on calibrated closure phase due to Strehl ratio variation (in rad)
  strehl_L_clo_AT =18.4e-3 
  strehl_L_clo_UT =18.4e-3
  strehl_M_clo_AT =12.2e-3 
  strehl_M_clo_UT =12.2e-3 
  strehl_N_clo_AT =11e-3 
  strehl_N_clo_UT =11e-3 

  ;Error on calibrated closure phase due to detection effect (with BCD)
  det_L_clo_AT = 5e-3
  det_L_clo_UT = 5e-3
  det_M_clo_AT = 5e-3
  det_M_clo_UT = 5e-3
  det_N_clo_AT = 5e-3
  det_N_clo_UT = 5e-3

  ;Error on calibrated closure phase due to detection effect (without BCD)
  ;det_L_clo_AT = 20e-3
  ;det_L_clo_UT = 20e-3
  ;det_M_clo_AT = 20e-3
  ;det_M_clo_UT = 20e-3
  ;det_N_clo_AT = 20e-3
  ;det_N_clo_UT = 20e-3


;-----------------------------------------------------------------------------
;Error contributions (instrument+calibration) on calibrated differential phase
;-----------------------------------------------------------------------------

  ;Error on calibrated differential phase due to Pinhole overlap (in rad)
  pine_L_phi_AT = 3.5e-3
  pine_L_phi_UT = 3.5e-3
  pine_M_phi_AT = 2.1e-3
  pine_M_phi_UT = 2.1e-3
  pine_N_phi_AT = 3.5e-3
  pine_N_phi_UT = 3.5e-3

  ;Error on calibrated differential phase due to Strehl ratio variation (in rad)
  strehl_L_phi_AT = 10.6e-3 
  strehl_L_phi_UT = 10.6e-3
  strehl_M_phi_AT = 7.1e-3 
  strehl_M_phi_UT = 7.1e-3 
  strehl_N_phi_AT = 6.4e-3
  strehl_N_phi_UT = 6.4e-3

  ;Error on calibrated differential phase due to OPD jitter
  opd_jitter_L_phi_AT = 4.2e-3
  opd_jitter_L_phi_UT = 12e-3
  opd_jitter_M_phi_AT = 2.8e-3
  opd_jitter_M_phi_UT = 7.8e-3
  opd_jitter_N_phi_AT = 14.1e-3
  opd_jitter_N_phi_UT = 18.4e-3

  ;Error on calibrated differential phase due to chromatic effect (without BCD)
  ;chro_L_phi_AT = 50e-3
  ;chro_L_phi_UT = 50e-3
  ;chro_M_phi_AT = 50e-3
  ;chro_M_phi_UT = 50e-3
  ;chro_N_phi_AT = 50e-3
  ;chro_N_phi_UT = 50e-3

  ;Error on calibrated differential phase due to chromatic effect (with BCD)
  chro_L_phi_AT = 15e-3
  chro_L_phi_UT = 15e-3
  chro_M_phi_AT = 15e-3
  chro_M_phi_UT = 15e-3
  chro_N_phi_AT = 15e-3
  chro_N_phi_UT = 15e-3

;return
;end

;
;>>>>>>>>>>>>> End of input variable declaration (Do not modify this line!)<<<<<
;

